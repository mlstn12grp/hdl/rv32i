--------------------------------------------------------------------------------
--! @file register_file.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity register_file is
	generic(
		REGISTER_WIDTH	:	positive	:= 32;
		REGISTER_COUNT	:	positive	:= 32
	);
	port(
		clk				: in  std_logic;
		rst				: in  std_logic;
		read_index_1	: in  natural range 0 to (REGISTER_COUNT - 1);
		read_index_2	: in  natural range 0 to (REGISTER_COUNT - 1);
		read_data_1		: out std_logic_vector((REGISTER_WIDTH - 1) downto 0);
		read_data_2		: out std_logic_vector((REGISTER_WIDTH - 1) downto 0);
		write_en		: in  std_logic;
		write_index		: in  natural range 0 to (REGISTER_COUNT - 1);
		write_data		: in  std_logic_vector((REGISTER_WIDTH - 1) downto 0)
	);
end entity register_file;

architecture arch of register_file is

	subtype word_t is std_logic_vector((REGISTER_WIDTH - 1) downto 0);
	type q_vector_t is array(natural range 0 to (REGISTER_COUNT - 1)) of word_t;

	signal en		: std_logic_vector(0 to (REGISTER_COUNT - 1));
	signal q_vector	: q_vector_t;

begin

	generate_register_file:
	for i in 0 to (REGISTER_COUNT - 1) generate
		register_zero: if i = 0 generate
			reg_inst : entity work.reg(arch)
				generic map(
					WIDTH	=> REGISTER_WIDTH
				)
				port map(
					clk	=> clk,
					rst => rst,
					en	=> en(i),
					d	=> write_data,
					q	=> open
				);
				q_vector(i)	<= (others => '0');
		end generate;

		register_one_and_up: if i /= 0 generate
			reg_inst : entity work.reg(arch)
				generic map(
					WIDTH	=> REGISTER_WIDTH
				)
				port map(
					clk	=> clk,
					rst => rst,
					en	=> en(i),
					d	=> write_data,
					q	=> q_vector(i)
				);
		end generate;
	end generate;

	read_data_proc : process (q_vector, read_index_1, read_index_2)
	begin
		read_data_1	<= q_vector(read_index_1);
		read_data_2	<= q_vector(read_index_2);
	end process;

	write_data_proc : process (write_index, write_en)
	begin
		en				<= (others => '0');
		if write_en = '1' then
			en(write_index)	<= '1';
		end if;
	end process;

end architecture;

architecture fpga of register_file is
	type mem_type is array (0 to (REGISTER_COUNT - 1)) of std_logic_vector(REGISTER_WIDTH - 1 downto 0);
	signal mem	: mem_type;
begin
	process (clk, rst) -- Write memory.
	begin
		if rst = '1' then
			mem	<= (others => (others => '0'));
		elsif rising_edge(clk) then
			if (write_en = '1') and not (write_index = 0) then
				mem(write_index) <= write_data;
				-- Using write address bus.
			end if;
		end if;
	end process;

	process (read_index_1, read_index_2)
	begin
		read_data_1 <= mem(read_index_1);
		read_data_2 <= mem(read_index_2);
	end process;
end architecture;
