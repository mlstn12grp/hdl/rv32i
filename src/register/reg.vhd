library ieee;
use ieee.std_logic_1164.all;

entity reg is
	generic(
		WIDTH			: positive;
		RESET_VECTOR	: std_logic_vector((WIDTH - 1) downto 0)	:= (others => '0')
	);
	port(
		clk	: in  std_logic;
		rst	: in  std_logic;
		en	: in  std_logic;
		d	: in  std_logic_vector((WIDTH - 1) downto 0);
		q	: out std_logic_vector((WIDTH - 1) downto 0)
	);
end entity reg;

architecture arch of reg is
begin

	reg_seq : process (clk, rst)
	begin
		if rst = '1' then
			q	<= RESET_VECTOR;
		elsif rising_edge(clk) and en = '1' then
			q	<= d;
		end if;
	end process;

end architecture;
