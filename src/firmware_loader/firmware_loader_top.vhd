library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity firmware_loader_top is
	port (
		rst			: in  std_logic;
		rx			: in  std_logic;
		tx			: out std_logic;
		debug_leds	: out std_logic_vector(1 to 8)
	);
end entity;

architecture structural of firmware_loader_top is

	component OSCH
		-- synthesis translate_off
		generic (
		NOM_FREQ	: string	:= "2.08"
		);
		-- synthesis translate_on
		port (
		stdby		: in  std_logic;
		osc			: out std_logic;
		sedstdby	: out std_logic
		);
	end component;

	attribute NOM_FREQ	: string;
	attribute NOM_FREQ of OSCH_inst	: label is "2.08";

	constant SYSTEM_CLOCK_FREQ			: positive	:= 2_080_000;
	constant FIRMWARE_LOADER_BAUD_RATE	: positive	:= 19_200;

	signal clk			: std_logic;

	signal proc_hold	: std_logic;
	signal proc_prog	: std_logic;
	signal proc_rst		: std_logic;
	signal proc_wr_en	: std_logic;

begin

	OSCH_inst : OSCH
		-- synthesis translate_off
		generic map (
			NOM_FREQ	=> "2.08"
		)
		-- synthesis translate_on
		port map (
			stdby		=> '0',
			osc			=> clk,
			sedstdby	=> open
		);

	FL_inst : entity work.firmware_loader (rtl)
		generic map (
			UART_BAUD_PERIOD	=> (SYSTEM_CLOCK_FREQ / FIRMWARE_LOADER_BAUD_RATE)
		)
		port map (
			clk					=> clk,
			rst					=> rst,
			uart_rx				=> rx,
			uart_tx				=> tx,
			proc_hold			=> proc_hold,
			proc_prog			=> proc_prog,
			proc_rst			=> proc_rst,
			proc_wr_en			=> proc_wr_en,
			instruction_address	=> open,
			instruction_data	=> open
		);

	debug_leds	<= (
		1 => rx,
		2 => tx,
		3 => proc_hold,
		4 => proc_prog,
		5 => proc_rst,
		6 => proc_wr_en,
		others => '1');


end architecture;
