--------------------------------------------------------------------------------
--! @file firmware_loader.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------
--! @todo Automatically calculate baud period from given baudrate and frequency
--! generics
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity firmware_loader is
	generic (
		UART_BAUD_PERIOD	: positive
	);
	port (
		clk						: in  std_logic;
		rst						: in  std_logic;
		uart_rx					: in  std_logic;
		uart_tx					: out std_logic;
		proc_hold				: out std_logic;
		proc_prog				: out std_logic;
		proc_rst				: out std_logic;
		proc_wr_en				: out std_logic;
		instruction_address		: out std_logic_vector(31 downto 0);
		instruction_data		: out std_logic_vector(31 downto 0)
	);
end entity;

architecture rtl of firmware_loader is
	type decode_state_t is (
		FL_DEC_IDLE,
		FL_DEC_HOLD_H,
		FL_DEC_HOLD_HO,
		FL_DEC_HOLD_HOL,
		FL_DEC_HOLD_HOLD,
		FL_DEC_HOLD_HOLD_CR,
		FL_DEC_PROG_P,
		FL_DEC_PROG_PR,
		FL_DEC_PROG_PRO,
		FL_DEC_PROG_PROG,
		FL_DEC_PROG_ADDR_1,
		FL_DEC_PROG_ADDR_2,
		FL_DEC_PROG_DATA_1,
		FL_DEC_PROG_DATA_2,
		FL_DEC_PROG_DATA_3,
		FL_DEC_PROG_DATA_4,
		FL_DEC_PROG_CHECKSUM,
		FL_DEC_PROG_TERM,
		FL_DEC_EXIT_E,
		FL_DEC_EXIT_EX,
		FL_DEC_EXIT_EXI,
		FL_DEC_EXIT_EXIT,
		FL_DEC_EXIT_EXIT_CR,
		FL_DEC_ERR_FOUND,
		FL_DEC_ERR,
		FL_DEC_CLEAR_UART_RX,
		FL_DEC_RESPONSE_OK_O,
		FL_DEC_RESPONSE_OK_OK,
		FL_DEC_RESPONSE_OK_OK_CR,
		FL_DEC_RESPONSE_OK_OK_CR_LF,
		FL_DEC_RESPONSE_ERR_E,
		FL_DEC_RESPONSE_ERR_ER,
		FL_DEC_RESPONSE_ERR_ERR,
		FL_DEC_RESPONSE_ERR_ERR_CR,
		FL_DEC_RESPONSE_ERR_ERR_CR_LF,
		FL_DEC_WAIT_UART_DONE
	);
	signal decode_state						: decode_state_t;
	signal decode_next_state				: decode_state_t;
	signal decode_post_shared_op_state		: decode_state_t;
	signal decode_post_shared_op_next_state	: decode_state_t;

	type state_t is (
		FL_IDLE,
		FL_HOLD,
		FL_PROG,
		FL_PROG_SAVE_ADDR_1,
		FL_PROG_SAVE_ADDR_2,
		FL_PROG_SAVE_DATA_1,
		FL_PROG_SAVE_DATA_2,
		FL_PROG_SAVE_DATA_3,
		FL_PROG_SAVE_DATA_4,
		FL_PROG_WRITE_INSTRUCTION,
		FL_RST
	);
	signal state		: state_t;
	signal next_state	: state_t;

	signal uart_rx_data			: std_logic_vector(8 downto 0);
	signal uart_rx_ready		: std_logic;
	signal uart_rx_clear		: std_logic;

	signal uart_tx_data			: std_logic_vector(8 downto 0);
	signal uart_tx_en			: std_logic;
	signal uart_tx_busy			: std_logic;

	signal decode_data			: std_logic_vector(7 downto 0);
	signal decode_character		: character;

	signal srecord_checksum		: std_logic_vector(7 downto 0);
begin

	UART_RX_inst : entity work.uart_rx (rtl)
		generic map (
			MAX_BAUD_PERIOD		=> UART_BAUD_PERIOD,
			SYNCHRONIZER_LENGTH	=> 3
			)
		port map (
			clk			=> clk,
			rst			=> rst,
			data_out	=> uart_rx_data,
			clear		=> uart_rx_clear,
			ready		=> uart_rx_ready,
			busy		=> open,
			done		=> open,
			rx			=> uart_rx,
			baud_period	=> UART_BAUD_PERIOD,
			data_length	=> uart_rx_data'length - 1,
			stop_bits	=> 2,
			parity		=> true,
			parity_odd	=> false
			);

	UART_TX_inst : entity work.uart_tx (rtl)
		generic map(
			MAX_BAUD_PERIOD	=> UART_BAUD_PERIOD
		)
		port map (
			clk			=> clk,
			rst			=> rst,
			data_in		=> uart_tx_data,
			en			=> uart_tx_en,
			ready		=> open,
			busy		=> uart_tx_busy,
			done		=> open,
			tx			=> uart_tx,
			baud_period	=> UART_BAUD_PERIOD,
			data_length	=> 8,
			stop_bits	=> 2,
			parity		=> true,
			parity_odd	=> false
		);

	----------------------------------------------------------------------------

	decode_data			<= uart_rx_data(decode_data'range);
	decode_character	<= character'val(to_integer(unsigned(decode_data)));

	decode_state_transition_comb : process (
		decode_state,
		decode_post_shared_op_state,
		decode_character,
		state,
		uart_rx_ready,
		uart_tx_busy
	)
	begin
		case decode_state is
			when FL_DEC_IDLE =>
				if uart_rx_ready = '1' then
					if decode_character = 'h' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_HOLD_H;
					elsif state = FL_HOLD and decode_character = 'p' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_PROG_P;
					elsif state = FL_HOLD and decode_character = 'e' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_EXIT_E;
					elsif decode_character = cr or decode_character = lf then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_IDLE;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_IDLE;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_HOLD_H =>
				if uart_rx_ready = '1' then
					if decode_character = 'o' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_HOLD_HO;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_HOLD_H;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_HOLD_HO =>
				if uart_rx_ready = '1' then
					if decode_character = 'l' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_HOLD_HOL;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_HOLD_HO;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_HOLD_HOL =>
				if uart_rx_ready = '1' then
					if decode_character = 'd' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_HOLD_HOLD;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_HOLD_HOL;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_HOLD_HOLD =>
				if uart_rx_ready = '1' then
					if decode_character = cr then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_HOLD_HOLD_CR;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_HOLD_HOLD;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_HOLD_HOLD_CR =>
				if uart_tx_busy = '0' then
					decode_next_state	<= FL_DEC_RESPONSE_OK_O;
				else
					decode_next_state	<= FL_DEC_HOLD_HOLD_CR;
				end if;
				decode_post_shared_op_next_state	<= decode_post_shared_op_state;
			when FL_DEC_PROG_P =>
				if uart_rx_ready = '1' then
					if decode_character = 'r' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_PROG_PR;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_PROG_P;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_PR =>
				if uart_rx_ready = '1' then
					if decode_character = 'o' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_PROG_PRO;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_PROG_PR;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_PRO =>
				if uart_rx_ready = '1' then
					if decode_character = 'g' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_PROG_PROG;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_PROG_PRO;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_PROG =>
				if uart_rx_ready = '1' then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_PROG_ADDR_1;
				else
					decode_next_state					<= FL_DEC_PROG_PROG;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_ADDR_1 =>
				if uart_rx_ready = '1' then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_PROG_ADDR_2;
				else
					decode_next_state					<= FL_DEC_PROG_ADDR_1;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_ADDR_2 =>
				if uart_rx_ready = '1' then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_PROG_DATA_1;
				else
					decode_next_state					<= FL_DEC_PROG_ADDR_2;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_DATA_1 =>
				if uart_rx_ready = '1' then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_PROG_DATA_2;
				else
					decode_next_state					<= FL_DEC_PROG_DATA_1;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_DATA_2 =>
				if uart_rx_ready = '1' then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_PROG_DATA_3;
				else
					decode_next_state					<= FL_DEC_PROG_DATA_2;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_DATA_3 =>
				if uart_rx_ready = '1' then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_PROG_DATA_4;
				else
					decode_next_state					<= FL_DEC_PROG_DATA_3;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_DATA_4 =>
				if uart_rx_ready = '1' then
					if decode_data = (not srecord_checksum) then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_PROG_CHECKSUM;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_PROG_DATA_4;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_CHECKSUM =>
				if uart_rx_ready = '1' then
					if decode_character = cr then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_PROG_TERM;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_PROG_CHECKSUM;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_PROG_TERM =>
				if uart_tx_busy = '0' then
					decode_next_state	<= FL_DEC_RESPONSE_OK_O;
				else
					decode_next_state	<= FL_DEC_PROG_TERM;
				end if;
				decode_post_shared_op_next_state	<= decode_post_shared_op_state;
			when FL_DEC_EXIT_E =>
				if uart_rx_ready = '1' then
					if decode_character = 'x' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_EXIT_EX;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_EXIT_E;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_EXIT_EX =>
				if uart_rx_ready = '1' then
					if decode_character = 'i' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_EXIT_EXI;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_EXIT_EX;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_EXIT_EXI =>
				if uart_rx_ready = '1' then
					if decode_character = 't' then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_EXIT_EXIT;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_EXIT_EXI;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_EXIT_EXIT =>
				if uart_rx_ready = '1' then
					if decode_character = cr then
						decode_next_state					<= FL_DEC_CLEAR_UART_RX;
						decode_post_shared_op_next_state	<= FL_DEC_EXIT_EXIT_CR;
					else
						decode_next_state					<= FL_DEC_ERR_FOUND;
						decode_post_shared_op_next_state	<= decode_post_shared_op_state;
					end if;
				else
					decode_next_state					<= FL_DEC_EXIT_EXIT;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_EXIT_EXIT_CR =>
				if uart_tx_busy = '0' then
					decode_next_state	<= FL_DEC_RESPONSE_OK_O;
				else
					decode_next_state	<= FL_DEC_EXIT_EXIT_CR;
				end if;
				decode_post_shared_op_next_state	<= decode_post_shared_op_state;
			when FL_DEC_ERR_FOUND	=>
				decode_next_state					<= FL_DEC_CLEAR_UART_RX;
				decode_post_shared_op_next_state	<= FL_DEC_ERR;
			when FL_DEC_ERR =>
				if uart_rx_ready = '1' and decode_character = cr then
					decode_next_state					<= FL_DEC_CLEAR_UART_RX;
					decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_ERR_E;
				else
					decode_next_state					<= FL_DEC_ERR;
					decode_post_shared_op_next_state	<= decode_post_shared_op_state;
				end if;
			when FL_DEC_CLEAR_UART_RX =>
				decode_next_state					<= decode_post_shared_op_state;
				decode_post_shared_op_next_state	<= decode_post_shared_op_state;
			when FL_DEC_RESPONSE_OK_O =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_OK_OK;
			when FL_DEC_RESPONSE_OK_OK =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_OK_OK_CR;
			when FL_DEC_RESPONSE_OK_OK_CR =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_OK_OK_CR_LF;
			when FL_DEC_RESPONSE_OK_OK_CR_LF =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_IDLE;
			when FL_DEC_RESPONSE_ERR_E =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_ERR_ER;
			when FL_DEC_RESPONSE_ERR_ER =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_ERR_ERR;
			when FL_DEC_RESPONSE_ERR_ERR =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_ERR_ERR_CR;
			when FL_DEC_RESPONSE_ERR_ERR_CR =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_RESPONSE_ERR_ERR_CR_LF;
			when FL_DEC_RESPONSE_ERR_ERR_CR_LF =>
				decode_next_state					<= FL_DEC_WAIT_UART_DONE;
				decode_post_shared_op_next_state	<= FL_DEC_IDLE;
			when FL_DEC_WAIT_UART_DONE =>
				if uart_tx_busy = '0' then
					decode_next_state	<= decode_post_shared_op_state;
				else
					decode_next_state	<= FL_DEC_WAIT_UART_DONE;
				end if;
				decode_post_shared_op_next_state	<= decode_post_shared_op_state;
		end case;
	end process;

	decode_state_transition_seq : process (clk, rst)
	begin
		if rst = '1' then
			decode_state				<= FL_DEC_IDLE;
			decode_post_shared_op_state	<= FL_DEC_IDLE;
		elsif rising_edge(clk) then
			decode_state				<= decode_next_state;
			decode_post_shared_op_state	<= decode_post_shared_op_next_state;
		end if;
	end process;

	decode_fsm_output_seq : process (clk, rst)
	begin
		if rst = '1' then
			uart_rx_clear	<= '0';
			uart_tx_en		<= '0';
			uart_tx_data	<= (others => '0');
		elsif rising_edge(clk) then
			case decode_next_state is
				when FL_DEC_CLEAR_UART_RX =>
					uart_rx_clear	<= '1';
					uart_tx_en		<= '0';
					uart_tx_data	<= uart_tx_data;
				when FL_DEC_RESPONSE_OK_O =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos('O'), 8));
				when FL_DEC_RESPONSE_OK_OK =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos('K'), 8));
				when FL_DEC_RESPONSE_OK_OK_CR =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos(cr), 8));
				when FL_DEC_RESPONSE_OK_OK_CR_LF =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos(lf), 8));
				when FL_DEC_RESPONSE_ERR_E =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos('E'), 8));
				when FL_DEC_RESPONSE_ERR_ER | FL_DEC_RESPONSE_ERR_ERR =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos('R'), 8));
				when FL_DEC_RESPONSE_ERR_ERR_CR =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos(cr), 8));
				when FL_DEC_RESPONSE_ERR_ERR_CR_LF =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '1';
					uart_tx_data	<= "0" & std_logic_vector(to_unsigned(character'pos(lf), 8));
				when others =>
					uart_rx_clear	<= '0';
					uart_tx_en		<= '0';
					uart_tx_data	<= uart_tx_data;
			end case;
		end if;
	end process;

	----------------------------------------------------------------------------

	state_transition_comb : process (
		state,
		decode_state
	)
	begin
		case state is
			when FL_IDLE =>
				if decode_state = FL_DEC_HOLD_HOLD_CR then
					next_state	<= FL_HOLD;
				else
					next_state	<= FL_IDLE;
				end if;
			when FL_HOLD =>
				if decode_state = FL_DEC_PROG_PROG then
					next_state	<= FL_PROG;
				elsif decode_state = FL_DEC_EXIT_EXIT_CR then
					next_state	<= FL_RST;
				else
					next_state	<= FL_HOLD;
				end if;
			when FL_PROG =>
				if decode_state = FL_DEC_PROG_TERM then
					next_state	<= FL_PROG_WRITE_INSTRUCTION;
				elsif decode_state = FL_DEC_ERR then
					next_state	<= FL_HOLD;
				elsif decode_state = FL_DEC_CLEAR_UART_RX then
					case decode_post_shared_op_state is
						when FL_DEC_PROG_ADDR_1 =>
							next_state	<= FL_PROG_SAVE_ADDR_1;
						when FL_DEC_PROG_ADDR_2 =>
							next_state	<= FL_PROG_SAVE_ADDR_2;
						when FL_DEC_PROG_DATA_1 =>
							next_state	<= FL_PROG_SAVE_DATA_1;
						when FL_DEC_PROG_DATA_2 =>
							next_state	<= FL_PROG_SAVE_DATA_2;
						when FL_DEC_PROG_DATA_3 =>
							next_state	<= FL_PROG_SAVE_DATA_3;
						when FL_DEC_PROG_DATA_4 =>
							next_state	<= FL_PROG_SAVE_DATA_4;
						when others =>
							next_state	<= FL_PROG;
					end case;
				else
					next_state	<= FL_PROG;
				end if;
			when FL_PROG_SAVE_ADDR_1 | FL_PROG_SAVE_ADDR_2 | FL_PROG_SAVE_DATA_1 | FL_PROG_SAVE_DATA_2 | FL_PROG_SAVE_DATA_3 | FL_PROG_SAVE_DATA_4 =>
				next_state	<= FL_PROG;
			when FL_PROG_WRITE_INSTRUCTION =>
				next_state	<= FL_HOLD;
			when FL_RST =>
				next_state	<= FL_IDLE;
		end case;
	end process;

	state_transition_seq : process (clk, rst)
	begin
		if rst = '1' then
			state	<= FL_IDLE;
		elsif rising_edge(clk) then
			state	<= next_state;
		end if;
	end process;

	fsm_output_seq : process (clk, rst)
	begin
		if rst = '1' then
			proc_hold			<= '0';
			proc_prog			<= '0';
			proc_rst			<= '0';
			proc_wr_en			<= '0';
			instruction_address	<= (others => '0');
			instruction_data	<= (others => '0');
			srecord_checksum	<= x"07";
		elsif rising_edge(clk) then
			case next_state is
				when FL_HOLD =>
					proc_hold			<= '1';
					proc_prog			<= '0';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data;
					srecord_checksum	<= x"07";
				when FL_PROG =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data;
					srecord_checksum	<= srecord_checksum;
				when FL_PROG_SAVE_ADDR_1 =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address(31 downto 16) & decode_data & instruction_address(7 downto 0);
					instruction_data	<= instruction_data;
					srecord_checksum	<= std_logic_vector(unsigned(srecord_checksum) + unsigned(decode_data));
				when FL_PROG_SAVE_ADDR_2 =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address(31 downto 8) & decode_data;
					instruction_data	<= instruction_data;
					srecord_checksum	<= std_logic_vector(unsigned(srecord_checksum) + unsigned(decode_data));
				when FL_PROG_SAVE_DATA_1 =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data(31 downto 8) & decode_data;
					srecord_checksum	<= std_logic_vector(unsigned(srecord_checksum) + unsigned(decode_data));
				when FL_PROG_SAVE_DATA_2 =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data(31 downto 16) & decode_data & instruction_data(7 downto 0);
					srecord_checksum	<= std_logic_vector(unsigned(srecord_checksum) + unsigned(decode_data));
				when FL_PROG_SAVE_DATA_3 =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data(31 downto 24) & decode_data & instruction_data(15 downto 0);
					srecord_checksum	<= std_logic_vector(unsigned(srecord_checksum) + unsigned(decode_data));
				when FL_PROG_SAVE_DATA_4 =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= decode_data & instruction_data(23 downto 0);
					srecord_checksum	<= std_logic_vector(unsigned(srecord_checksum) + unsigned(decode_data));
				when FL_PROG_WRITE_INSTRUCTION =>
					proc_hold			<= '1';
					proc_prog			<= '1';
					proc_rst			<= '0';
					proc_wr_en			<= '1';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data;
					srecord_checksum	<= srecord_checksum;
				when FL_RST =>
					proc_hold			<= '1';
					proc_prog			<= '0';
					proc_rst			<= '1';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data;
					srecord_checksum	<= x"07";
				when others =>
					proc_hold			<= '0';
					proc_prog			<= '0';
					proc_rst			<= '0';
					proc_wr_en			<= '0';
					instruction_address	<= instruction_address;
					instruction_data	<= instruction_data;
					srecord_checksum	<= x"07";
			end case;
		end if;
	end process;

end architecture;
