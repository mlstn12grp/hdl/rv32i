library ieee;
use ieee.std_logic_1164.all;

package cpu_utils_pkg is

  function swap_endianness (word : in std_logic_vector(31 downto 0); debug : in boolean := false) return std_logic_vector;

end cpu_utils_pkg;

package body cpu_utils_pkg is

  -- swap endianness of 32-bit word
  function swap_endianness (word : in std_logic_vector(31 downto 0); debug : in boolean := false) return std_logic_vector is
    variable swapped_word : std_logic_vector(31 downto 0);
  begin
    swapped_word := word(7 downto 0) &
                    word(15 downto 8) &
                    word(23 downto 16) &
                    word(31 downto 24);
    if debug then
      report "Swapped endianness of " & to_hstring(word) & " to " & to_hstring(swapped_word)
        severity note;
    end if;

    return swapped_word;
  end swap_endianness;

end cpu_utils_pkg;
