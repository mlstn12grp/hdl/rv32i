library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_isa_pkg.all;
use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;
use work.cpu_memory_pkg.all;
use work.cpu_instruction_decode_pkg.all;

entity instruction_decoder is
	port (
		binary_instruction	: in  instruction_word_t;
		decoded_instruction	: out instruction_t
	);
end entity instruction_decoder;

-- pragma translate_off
architecture simple of instruction_decoder is
begin
	decoded_instruction <= decode(binary_instruction => binary_instruction);
end architecture;
-- pragma translate_on

architecture rtl of instruction_decoder is

	signal operation_type	: operation_type_t;
	signal operation		: operation_t;

	signal opcode_1_to_0	: std_logic_vector(1 downto 0);
	signal opcode_4_to_2	: std_logic_vector(4 downto 2);
	signal opcode_6_to_5	: std_logic_vector(6 downto 5);

	signal funct3			: std_logic_vector(2 downto 0);
	signal funct7			: std_logic_vector(6 downto 0);

begin

	opcode_1_to_0	<= binary_instruction(1 downto 0);
	opcode_4_to_2	<= binary_instruction(4 downto 2);
	opcode_6_to_5	<= binary_instruction(6 downto 5);

	funct3			<= binary_instruction(14 downto 12);
	funct7			<= binary_instruction(31 downto 25);

	decode_operation_type : process (opcode_1_to_0, opcode_4_to_2, opcode_6_to_5)
	begin
		case opcode_1_to_0 is
			when "11" =>
				case opcode_6_to_5 is
					when "00" =>
						case opcode_4_to_2 is
							when "000" =>
								operation_type	<= loads;
							when "011" =>
								operation_type	<= fences;
							when "100" =>
								operation_type	<= arithmetic_imm;
							when "101" =>
								operation_type	<= AUIPC;
							when others =>
								operation_type	<= unknown;
						end case;
					when "01" =>
						case opcode_4_to_2 is
							when "000" =>
								operation_type	<= stores;
							when "100" =>
								operation_type	<= arithmetic_reg;
							when "101" =>
								operation_type	<= LUI;
							when others =>
								operation_type	<= unknown;
						end case;
					when "10" =>
						operation_type	<= unknown;
					when "11" =>
						case opcode_4_to_2 is
							when "000" =>
								operation_type	<= branches;
							when "001" =>
								operation_type	<= JALR;
							when "011" =>
								operation_type	<= JAL;
							when "100" =>
								operation_type	<= system;
							when others =>
								operation_type	<= unknown;
						end case;
					when others =>
						operation_type	<= unknown;
				end case;
			when others => -- compressed ISA
				operation_type	<= unknown;
		end case;
	end process;

	decode_operation : process (operation_type, funct3, funct7)
	begin
		case operation_type is
			when unknown =>
				operation <= unknown;
			when loads =>
				case funct3 is
					when "000" =>
						operation	<= op_lb;
					when "001" =>
						operation	<= op_lh;
					when "010" =>
						operation	<= op_lw;
					when "100" =>
						operation	<= op_lbu;
					when "101" =>
						operation	<= op_lhu;
					when others =>
						operation	<= unknown;
				end case;
			when stores =>
				case funct3 is
					when "000" =>
						operation	<= op_sb;
					when "001" =>
						operation	<= op_sh;
					when "010" =>
						operation	<= op_sw;
					when others =>
						operation	<= unknown;
				end case;
			when branches =>
				case funct3 is
					when "000" =>
						operation	<= op_beq;
					when "001" =>
						operation	<= op_bne;
					when "100" =>
						operation	<= op_blt;
					when "101" =>
						operation	<= op_bge;
					when "110" =>
						operation	<= op_bltu;
					when "111" =>
						operation	<= op_bgeu;
					when others =>
						operation	<= unknown;
				end case;
			when JALR =>
				operation	<= op_jalr;
			when fences =>
				case funct3 is
					when "000" =>
						operation	<= op_fence;
					when "001" =>
						operation	<= op_fenceI;
					when others =>
						operation	<= unknown;
				end case;
			when JAL =>
				operation	<= op_jal;
			when arithmetic =>
				operation	<= unknown;
			when arithmetic_imm =>
				case funct3 is
					when "000" =>
						operation	<= op_addi;
					when "001" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_slli;
							when others =>
								operation	<= unknown;
						end case;
					when "010" =>
						operation	<= op_slti;
					when "011" =>
						operation	<= op_sltiu;
					when "100" =>
						operation	<= op_xori;
					when "101" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_srli;
							when "0100000" =>
								operation	<= op_srai;
							when others =>
								operation	<= unknown;
						end case;
					when "110" =>
						operation	<= op_ori;
					when "111" =>
						operation	<= op_andi;
					when others =>
						operation	<= unknown;
				end case;
			when arithmetic_reg =>
				case funct3 is
					when "000" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_add;
							when "0100000" =>
								operation	<= op_sub;
							when others =>
								operation	<= unknown;
						end case;
					when "001" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_sll;
							when others =>
								operation	<= unknown;
						end case;
					when "010" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_slt;
							when others =>
								operation	<= unknown;
						end case;
					when "011" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_sltu;
							when others =>
								operation	<= unknown;
						end case;
					when "100" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_xor;
							when others =>
								operation	<= unknown;
						end case;
					when "101" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_srl;
							when "0100000" =>
								operation	<= op_sra;
							when others =>
								operation	<= unknown;
						end case;
					when "110" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_or;
							when others =>
								operation	<= unknown;
						end case;
					when "111" =>
						case funct7 is
							when "0000000" =>
								operation	<= op_and;
							when others =>
								operation	<= unknown;
						end case;
					when others =>
						operation	<= unknown;
				end case;
			when system =>
				case funct3 is
					when "000" =>
						operation	<= op_scall_sbreak;
					when "001" =>
						operation	<= op_csrrw;
					when "010" =>
						operation	<= op_csrrc;
					when "011" =>
						operation	<= op_csrrs;
					when "101" =>
						operation	<= op_csrrwi;
					when "110" =>
						operation	<= op_csrrci;
					when "111" =>
						operation	<= op_csrrsi;
					when others =>
						operation	<= unknown;
				end case;
			when AUIPC =>
				operation	<= op_auipc;
			when LUI =>
				operation	<= op_lui;
		end case;
	end process;

	decode_operands : process (operation, binary_instruction)
		variable composite_immediate	: std_ulogic_vector(31 downto 0)	:= (others => '0');
	begin
		decoded_instruction.destination_register	<= to_integer(unsigned(binary_instruction(11 downto 7)));
		decoded_instruction.source_register_1		<= to_integer(unsigned(binary_instruction(19 downto 15)));
		decoded_instruction.source_register_2		<= to_integer(unsigned(binary_instruction(24 downto 20)));
		case instruction_type_map(operation) is
			when I_type =>
				decoded_instruction.immediate_operand		<= to_integer(signed(binary_instruction(31 downto 20)));
			when S_type =>
				composite_immediate(11 downto 0)			:= binary_instruction(31 downto 25) & binary_instruction(11 downto 7);
				decoded_instruction.immediate_operand		<= to_integer(signed(composite_immediate(11 downto 0)));
			when SB_type =>
				composite_immediate(12 downto 0)			:= binary_instruction(31 downto 31) & binary_instruction(7 downto 7) & binary_instruction(30 downto 25) & binary_instruction(11 downto 8) & "0";
				decoded_instruction.immediate_operand		<= to_integer(signed(composite_immediate(12 downto 0)));
			when U_type =>
				composite_immediate(19 downto 0)			:= binary_instruction(31 downto 12);
				decoded_instruction.immediate_operand		<= to_integer(signed(composite_immediate(19 downto 0)));
			when UJ_type =>
				composite_immediate(20 downto 0)			:= binary_instruction(31 downto 31) & binary_instruction(19 downto 12) & binary_instruction(20 downto 20) & binary_instruction(30 downto 21) & "0";
				decoded_instruction.immediate_operand		<= to_integer(signed(composite_immediate(20 downto 0)));
			when others =>
				decoded_instruction.immediate_operand		<= 0;
		end case;
	end process;

	decoded_instruction.operation	<= operation;

end architecture;
