library std;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;

use work.riscv_isa_pkg.all;
use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;
use work.cpu_utils_pkg.all;

package cpu_memory_pkg is

  -- types to represent a binary instruction memory
  subtype instruction_word_t is std_ulogic_vector((INSTRUCTION_WORD_WIDTH - 1) downto 0);
  type instruction_memory_binary_t is array(natural'left to (PROGRAM_MEMORY_SIZE - 1)) of instruction_word_t;

  -- type for an abstract instruction memory representation
  type instruction_memory_record_t is array(natural'left to (PROGRAM_MEMORY_SIZE - 1)) of instruction_t;

  -- type for an abstract data memory representation
  type data_memory_t is array(natural'left to (RAM_SIZE - 1)) of integer;

  ------------------------------------------------------------------------------

  impure function parse_srecord (
      filepath  : in string;
      debug     : in boolean := false
      )
      return instruction_memory_binary_t;

end cpu_memory_pkg;


package body cpu_memory_pkg is

  impure function parse_srecord (
      filepath  : in string;
      debug     : in boolean := false
      )
      return instruction_memory_binary_t is

    file      srec_file : text;
    variable  srec_line : line;
    variable  srec_cmd  : string(1 to 2);
    variable  srec_len  : string(1 to 2);
    variable  srec_addr : string(1 to 4);
    variable  mem       : instruction_memory_binary_t;
    variable  index     : natural := mem'left;
  begin
    if debug then
      report "Parsing S-Record file"
        severity note;
      report "Opening file " & filepath
        severity note;
    end if;
    file_open(srec_file, filepath, read_mode);

    if debug then
      report "Enter line parsing loop"
        severity note;
    end if;
    while (not endfile(srec_file) and index <= mem'right) loop

      if debug then
        report "Reading line from file"
          severity note;
      end if;
      readline(f => srec_file, l => srec_line);

      case (srec_line.all(1 to 2)) is
        when "S0" =>
          report "Found S-Record Header: " & srec_line.all
            severity note;
        when "S1" =>
          if debug then
            report "Found S1 entry: " & srec_line.all
              severity note;
          end if;
          -- Assert that entry is exactly 4 bytes
          assert (srec_line.all(3 to 4) = "07")
            report "Found entry size other than 4 bytes. Unable to process for now"
              severity error;
          -- remove srec command
          read(l => srec_line, value => srec_cmd);
          if debug then
            report "Removal of SREC command; remainder of line: " & srec_line.all
              severity note;
          end if;
          -- remove srec length
          read(l => srec_line, value => srec_len);
          if debug then
            report "Removal of SREC entry length; remainder of line: " & srec_line.all
              severity note;
          end if;
          -- remove srec target address
          read(l => srec_line, value => srec_addr);
          if debug then
            report "Removal of SREC target address; remainder of line: " & srec_line.all
              severity note;
          end if;
          -- read hex data into std_ulogic_vector field in imem
          ieee.std_logic_1164.hread(l => srec_line, value => mem(index));
          if debug then
            report "Read hexadecimal data into mem(" & index'subtype'image(index) & "): 0x" & to_hstring(mem(index))
              severity note;
          end if;
          index := index + 1;
        when "S9" =>
          if debug then
          report "Found end of S-Record: " & srec_line.all & "; Exiting"
            severity note;
          end if;
          exit;
        when others =>
          report "Found unknown / unimplemented S-Record entry: " & srec_line.all & "; Exiting"
            severity error;
          exit;
      end case;
    end loop;

    if debug then
      report "Closing S-Record file"
        severity note;
    end if;
    file_close(srec_file);

    if debug then
        report "Swapping endianness of data words"
            severity note;
    end if;
    for i in mem'range loop
        mem(i) := swap_endianness(mem(i));
    end loop;

    if debug then
      report "End of S-Record parsing procedure"
        severity note;
    end if;

    return mem;
  end parse_srecord;

end cpu_memory_pkg;
