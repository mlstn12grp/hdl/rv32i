library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cpu_memory_pkg.all;
use work.cpu_utils_pkg.all;

entity instruction_memory is
	generic (
		SYNTHESIS	: boolean	:= false
	);
	port (
		rst     : in std_logic;
		clk     : in std_logic;
		address : in natural range instruction_memory_binary_t'range;
		data    : out instruction_word_t
	);
end instruction_memory;

architecture structural of instruction_memory is
	constant instruction_rom : instruction_memory_binary_t := parse_srecord(filepath => "/mnt/data/Development/Hardware/riscv-soc/software/test.srec");
begin
	data_out : process (clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				data <= (others => '0');
			else
				data <= instruction_rom(address);
			end if;
		end if;
	end process;
end structural;

architecture fpga of instruction_memory is
	component instruction_rom
        port (Address : in std_logic_vector(7 downto 0);
        OutClock: in std_logic; OutClockEn: in std_logic;
        Reset: in std_logic; Q : out std_logic_vector(31 downto 0)
    );
    end component;

	for ebr_rom_inst_sim : instruction_rom use configuration work.Structure_CON;

	signal address_bitvector	: std_logic_vector(7 downto 0);
	signal out_clk_en			: std_logic;
	signal big_endian_data		: instruction_word_t;
begin
	ebr_rom_inst_sim : instruction_rom
		port map(
			Address		=> address_bitvector,
			OutClock	=> clk,
			OutClockEn	=> out_clk_en,
			Reset		=> rst,
			Q			=> big_endian_data
		);

	out_clk_en			<= not rst;
	address_bitvector	<= std_logic_vector(to_unsigned(address, address_bitvector'length));
	data				<= swap_endianness(big_endian_data);
end architecture;
