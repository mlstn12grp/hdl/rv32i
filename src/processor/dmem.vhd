library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram is
	generic (
		ADDR_WIDTH	: natural	:= 8;
		DATA_WIDTH	: natural	:= 8
	);
	port (
		write_en	: in  std_logic;
		waddr		: in  std_logic_vector((ADDR_WIDTH - 1) downto 0);
		wclk		: in  std_logic;
		raddr		: in  std_logic_vector((ADDR_WIDTH - 1) downto 0);
		rclk		: in  std_logic;
		din			: in  std_logic_vector (DATA_WIDTH - 1 downto 0);
		dout		: out std_logic_vector (DATA_WIDTH - 1 downto 0)
	);
end ram;

architecture rtl of ram is
	type mem_type is array (0 to (2**ADDR_WIDTH - 1)) of std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal mem				: mem_type;

	attribute syn_ramstyle	: string;
	attribute syn_ramstyle of mem: signal is "no_rw_check";
begin
	process (wclk) -- Write memory.
	begin
		if rising_edge(wclk) then
			if (write_en = '1') then
				mem(to_integer(unsigned(waddr))) <= din;
				-- Using write address bus.
			end if;
		end if;
	end process;

	-- process (rclk) -- Read memory.
	-- begin
	-- 	if rising_edge(rclk) then
	-- 		dout <= mem(raddr);
	-- 		-- Using read address bus.
	-- 	end if;
	-- end process;

	process (raddr)
	begin
		-- pragma translate_off
		report "dmem raddr: " & natural'image(to_integer(unsigned(raddr)));
		-- pragma translate_on
		dout <= mem(to_integer(unsigned(raddr)));
	end process;
end rtl;
