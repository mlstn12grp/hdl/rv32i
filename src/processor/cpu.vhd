--------------------------------------------------------------------------------
--! @file cpu.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------
--! @todo Add more comprehensive logging and a log level generic from VUnit
--
--! @todo Implement missing load and store instructions in architecture
--! behavioral
--
--! @todo Rename cpu to processor
--
--! @todo Migrate structural architecture to a configuration-based style that
--! allows easy differentiation of simulation models, generic synthesizeable
--! descriptions and platform-specific descriptions.
--
--! @todo Add test that simulates that firmware is loaded through firmware
--! loader
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_isa_pkg.all;
use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;
use work.cpu_register_pkg.all;
use work.cpu_memory_pkg.all;
use work.cpu_utils_pkg.all;
use work.cpu_instruction_decode_pkg.all;
use work.cpu_test_pkg.all;

entity cpu is
	generic (
		SYNTHESIS				: boolean			:= false;
		register_file_init		: register_file_t	:= (others => 0);
		instruction_memory_init	: instruction_memory_record_t	:= (others => (unknown, 0, 0, 0, 0));
		data_memory_init		: data_memory_t		:= (others => 0)
	);
	port (
		clk					: in  std_ulogic;
		rst					: in  reset_vector_t;
		hold				: in  std_ulogic;
		debug_clk			: in  std_ulogic;
		debug_wr_address	: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
		debug_wr_data		: in  instruction_word_t;
		debug_wr_en			: in  std_ulogic;
		test_output			: out test_output_t
	);
end entity cpu;

-- an abstract model of the RV32I ISA intended for simulation only
-- synthesis translate_off
architecture behavioral of cpu is

	----------------------------------------------------------------------------

	constant instruction_memory : instruction_memory_record_t := instruction_memory_init;

	signal program_counter : program_counter_t := 0;

	signal register_file : register_file_t := register_file_init;

	signal data_memory : data_memory_t := data_memory_init;

begin
	cpu_process : process (rst, clk)
		variable current_instruction : instruction_t;
		variable local_test_output : test_output_t;
	begin

		if rst.global = '1' then
			program_counter	<= 0;
			register_file	<= (others => 0);
			data_memory		<= (others => 0);

		elsif rising_edge(clk) then

			test_output.test_end	<= false;

			if hold = '0' then

				report "pc: " & natural'image(program_counter);
				report "load instruction";
				current_instruction := instruction_memory((program_counter / 4));
				case current_instruction.operation is
					------------------------------------------------------------
					-- arithmetic on registers
					----------------------------------------------------------------
					when op_add =>
						register_file(current_instruction.destination_register) <= register_file(current_instruction.source_register_1) + register_file(current_instruction.source_register_2);
						program_counter <= program_counter + 4;
					when op_sub =>
						register_file(current_instruction.destination_register) <= register_file(current_instruction.source_register_1) - register_file(current_instruction.source_register_2);
						program_counter <= program_counter + 4;
					when op_sll =>
						if register_file(current_instruction.source_register_2) > (DATA_WORD_WIDTH - 1) then
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sll (DATA_WORD_WIDTH - 1));
						else
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sll register_file(current_instruction.source_register_2));
						end if;
						program_counter <= program_counter + 4;
					when op_srl =>
						if register_file(current_instruction.source_register_2) > (DATA_WORD_WIDTH - 1) then
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) srl (DATA_WORD_WIDTH - 1));
						else
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) srl register_file(current_instruction.source_register_2));
						end if;
						program_counter <= program_counter + 4;
					when op_sra =>
						if register_file(current_instruction.source_register_2) > (DATA_WORD_WIDTH - 1) then
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sra (DATA_WORD_WIDTH - 1));
						else
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sra register_file(current_instruction.source_register_2));
						end if;
						program_counter <= program_counter + 4;
					when op_and =>
						register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) and to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH));
						program_counter <= program_counter + 4;
					when op_or =>
						register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) or to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH));
						program_counter <= program_counter + 4;
					when op_xor =>
						register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) xor to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH));
						program_counter <= program_counter + 4;
					when op_slt =>
						if to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) < to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH) then
							register_file(current_instruction.destination_register) <= 1;
						else
							register_file(current_instruction.destination_register) <= 0;
						end if;
						program_counter <= program_counter + 4;
					when op_sltu =>
						if unsigned(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH)) < unsigned(to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH)) then
							register_file(current_instruction.destination_register) <= 1;
						else
							register_file(current_instruction.destination_register) <= 0;
						end if;
						program_counter <= program_counter + 4;
					------------------------------------------------------------
					-- arithmetic with immediate operand
					----------------------------------------------------------------
					when op_addi =>
						register_file(current_instruction.destination_register) <= register_file(current_instruction.source_register_1) + to_integer(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH));
						program_counter <= program_counter + 4;
					when op_slli =>
						if current_instruction.immediate_operand > (DATA_WORD_WIDTH - 1) then
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sll (DATA_WORD_WIDTH - 1));
						else
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sll current_instruction.immediate_operand);
						end if;
						program_counter <= program_counter + 4;
					when op_srli =>
						if current_instruction.immediate_operand > (DATA_WORD_WIDTH - 1) then
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) srl (DATA_WORD_WIDTH - 1));
						else
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) srl current_instruction.immediate_operand);
						end if;
						program_counter <= program_counter + 4;
					when op_srai =>
						if current_instruction.immediate_operand > (DATA_WORD_WIDTH - 1) then
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sra (DATA_WORD_WIDTH - 1));
						else
							register_file(current_instruction.destination_register) <= to_integer(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) sra current_instruction.immediate_operand);
						end if;
						program_counter <= program_counter + 4;
					when op_andi =>
						register_file(current_instruction.destination_register) <= to_integer(signed(to_unsigned(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) and to_unsigned(current_instruction.immediate_operand, DATA_WORD_WIDTH)));
						program_counter <= program_counter + 4;
					when op_ori =>
						register_file(current_instruction.destination_register) <= to_integer(signed(to_unsigned(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) or to_unsigned(current_instruction.immediate_operand, DATA_WORD_WIDTH)));
						program_counter <= program_counter + 4;
					when op_xori =>
						register_file(current_instruction.destination_register) <= to_integer(signed(to_unsigned(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) xor to_unsigned(current_instruction.immediate_operand, DATA_WORD_WIDTH)));
						program_counter <= program_counter + 4;
					when op_slti =>
						if to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH) < current_instruction.immediate_operand then
							register_file(current_instruction.destination_register) <= 1;
						else
							register_file(current_instruction.destination_register) <= 0;
						end if;
						program_counter <= program_counter + 4;
					when op_sltiu =>
						if unsigned(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH)) < unsigned(to_unsigned(current_instruction.immediate_operand, DATA_WORD_WIDTH)) then
							register_file(current_instruction.destination_register) <= 1;
						else
							register_file(current_instruction.destination_register) <= 0;
						end if;
						program_counter <= program_counter + 4;
					when op_lw =>
						register_file(current_instruction.destination_register) <= data_memory((register_file(current_instruction.source_register_1) + current_instruction.immediate_operand) / 4);
						program_counter <= program_counter + 4;
					when op_sw =>
						report "sw";
						report "sw index: " & integer'image((register_file(current_instruction.source_register_1) + current_instruction.immediate_operand) / 4);
						data_memory((register_file(current_instruction.source_register_1) + current_instruction.immediate_operand) / 4) <= register_file(current_instruction.source_register_2);
						program_counter <= program_counter + 4;
					------------------------------------------------------------
					-- branches
					----------------------------------------------------------------
					when op_beq =>
						if register_file(current_instruction.source_register_1) = register_file(current_instruction.source_register_2) then
						  program_counter <= program_counter + current_instruction.immediate_operand;
						else
						  program_counter <= program_counter + 4;
						end if;
					when op_bne =>
						if register_file(current_instruction.source_register_1) /= register_file(current_instruction.source_register_2) then
						  program_counter <= program_counter + current_instruction.immediate_operand;
						else
						  program_counter <= program_counter + 4;
						end if;
					when op_blt =>
						if register_file(current_instruction.source_register_1) < register_file(current_instruction.source_register_2) then
						  program_counter <= program_counter + current_instruction.immediate_operand;
						else
						  program_counter <= program_counter + 4;
						end if;
					when op_bltu =>
						if unsigned(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH)) < unsigned(to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH)) then
						  program_counter <= program_counter + current_instruction.immediate_operand;
						else
						  program_counter <= program_counter + 4;
						end if;
					when op_bge =>
						if register_file(current_instruction.source_register_1) >= register_file(current_instruction.source_register_2) then
						  program_counter <= program_counter + current_instruction.immediate_operand;
						else
						  program_counter <= program_counter + 4;
						end if;
					when op_bgeu =>
						if unsigned(to_signed(register_file(current_instruction.source_register_1), DATA_WORD_WIDTH)) >= unsigned(to_signed(register_file(current_instruction.source_register_2), DATA_WORD_WIDTH)) then
						  program_counter <= program_counter + current_instruction.immediate_operand;
						else
						  program_counter <= program_counter + 4;
						end if;
					------------------------------------------------------------
					-- special
					----------------------------------------------------------------
					when op_lui =>
						register_file(current_instruction.destination_register) <= to_integer(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH) sll (32 - 20));
						program_counter <= program_counter + 4;
					when op_auipc =>
						register_file(current_instruction.destination_register) <= program_counter + to_integer(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH) sll (32 - 20));
						program_counter <= program_counter + to_integer(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH) sll (32 - 20));
					when op_jal =>
						register_file(current_instruction.destination_register) <= program_counter + 4;
						program_counter <= program_counter + current_instruction.immediate_operand;
						if current_instruction.immediate_operand = 0 then
							report "This is the end of the test";
							test_output.test_end	<= true;
						end if;
					when op_jalr =>
						register_file(current_instruction.destination_register) <= program_counter + 4;
						program_counter <= register_file(current_instruction.source_register_1) + current_instruction.immediate_operand;
					when unknown =>
						report "Found unknown operation at PC = " & program_counter'subtype'image(program_counter)
						  severity warning;
						program_counter <= instruction_memory'high + 1;
					when others =>
						report "Unimplemented instruction: " & operation_t'image(current_instruction.operation)
							severity failure;
				end case;

			end if;

		end if;

		-- This models behavior of register zero.
		register_file(0)	<= 0;
	end process;

	test_output.program_counter <= program_counter;
	test_output.register_file <= register_file;
	test_output.data_memory <= data_memory;

end architecture behavioral;
-- synthesis translate_on

architecture structural of cpu is

	component alu
		port (
			hold						: in  std_logic;
			program_counter				: in  program_counter_t;
			program_counter_next		: out program_counter_t;
			current_instruction			: in  instruction_t;
			source_register_1_data		: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
			source_register_2_data		: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
			register_write_en			: out std_logic;
			destination_register_data	: out std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
			ram_write_en				: out std_logic;
			ram_write_address			: out std_logic_vector((RAM_ADDRESS_WIDTH - 1) downto 0);
			ram_read_address			: out std_logic_vector((RAM_ADDRESS_WIDTH - 1) downto 0);
			ram_write_data				: out std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
			ram_read_data				: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
			test_end					: out boolean
		);
	end component;

	component instruction_memory
		port (
			clk     			: in  std_logic;
			address 			: in  natural range instruction_memory_binary_t'range;
			data    			: out instruction_word_t;
			debug_clk			: in  std_ulogic;
			debug_wr_index		: in  natural range instruction_memory_binary_t'range;
			debug_wr_data		: in  instruction_word_t;
			debug_wr_en			: in  std_ulogic
		);
	end component;

	----------------------------------------------------------------------------

	signal reset_unit_vector			: reset_unit_vector_t;
	signal program_counter				: program_counter_t;
	signal program_counter_next			: program_counter_t;
	signal imem_address					: natural range 0 to (PROGRAM_MEMORY_SIZE - 1);
	signal current_instruction_word		: instruction_word_t;
	signal current_instruction			: instruction_t;
	signal register_write_en			: std_logic;
	signal source_register_1_data		: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal source_register_2_data		: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal destination_register_data	: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal ram_write_en					: std_logic;
	signal ram_write_address			: std_logic_vector((RAM_ADDRESS_WIDTH - 1) downto 0);
	signal ram_read_address				: std_logic_vector((RAM_ADDRESS_WIDTH - 1) downto 0);
	signal ram_write_data				: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal ram_read_data				: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);

	signal debug_wr_index				: natural range 0 to (PROGRAM_MEMORY_SIZE - 1);

	----------------------------------------------------------------------------

	constant imem_init : instruction_memory_binary_t := parse_srecord(filepath => "/mnt/data/Development/Hardware/riscv-soc/software/test.srec");

	----------------------------------------------------------------------------

	for alu_inst : alu
		use entity work.alu (logic)
			port map(
				hold						=> hold,
				program_counter				=> program_counter,
				program_counter_next		=> program_counter_next,
				current_instruction			=> current_instruction,
				source_register_1_data		=> source_register_1_data,
				source_register_2_data		=> source_register_2_data,
				register_write_en			=> register_write_en,
				destination_register_data	=> destination_register_data,
				ram_write_en				=> ram_write_en,
				ram_write_address			=> ram_write_address,
				ram_read_address			=> ram_read_address,
				ram_write_data				=> ram_write_data,
				ram_read_data				=> ram_read_data,
				test_end					=> test_output.test_end
			);

begin
	reset_unit_vector.register_file		<= rst.global or rst.firmware_loader;
	reset_unit_vector.program_counter	<= rst.global or rst.firmware_loader;

	-- SYNTHESIS = true: No initialization of instruction memory, so that LSE
	-- is fine.
	-- SYNTHESIS = false: Instruction gets initialized with compiled program
	-- for simulation.
	imem_synth_sim : if SYNTHESIS generate
		-- For some reason, the Lattice Synthesis Engine cannot deal with the
		-- respective configuration specification.
		-- It complains about names exceeding their maximum length.
		-- So, direct instantiation of the entity had to be used instead.
		instruction_memory_inst : entity work.imem_dp (rtl)
			port map (
				addra		=> imem_address,
				write_ena	=> '0',
				clka		=> clk,
				dina		=> (others => '0'),
				douta		=> current_instruction_word,
				-------------------------------
				addrb		=> debug_wr_index,
				write_enb	=> debug_wr_en,
				clkb		=> debug_clk,
				dinb		=> debug_wr_data,
				doutb		=> open
			);
	else generate
		instruction_memory_inst : entity work.imem_dp (rtl)
			generic map (
				INIT_DATA	=> imem_init
			)
			port map (
				addra		=> imem_address,
				write_ena	=> '0',
				clka		=> clk,
				dina		=> (others => '0'),
				douta		=> current_instruction_word,
				-------------------------------
				addrb		=> debug_wr_index,
				write_enb	=> debug_wr_en,
				clkb		=> debug_clk,
				dinb		=> debug_wr_data,
				doutb		=> open
			);
	end generate;
	imem_address	<= to_integer(to_unsigned(program_counter_next, (PROGRAM_MEMORY_ADDRESS_WIDTH + 2))((PROGRAM_MEMORY_ADDRESS_WIDTH + 2 - 1) downto 2));
	debug_wr_index	<= to_integer(unsigned(debug_wr_address((PROGRAM_MEMORY_ADDRESS_WIDTH + 2 - 1) downto 2)));

	register_file_inst : entity work.register_file(fpga)
		generic map(
			REGISTER_WIDTH	=> DATA_WORD_WIDTH,
			REGISTER_COUNT	=> REGISTER_FILE_SIZE
		)
		port map(
			clk				=> clk,
			rst				=> reset_unit_vector.register_file,
			read_index_1	=> current_instruction.source_register_1,
			read_index_2	=> current_instruction.source_register_2,
			read_data_1		=> source_register_1_data,
			read_data_2		=> source_register_2_data,
			write_en		=> register_write_en,
			write_index		=> current_instruction.destination_register,
			write_data		=> destination_register_data
		);

	ram_inst : entity work.ram(rtl)
		generic map(
			ADDR_WIDTH	=> RAM_ADDRESS_WIDTH,
			DATA_WIDTH	=> DATA_WORD_WIDTH
		)
		port map(
			write_en	=> ram_write_en,
			waddr		=> ram_write_address,
			wclk		=> clk,
			raddr		=> ram_read_address,
			rclk		=> clk,
			din			=> ram_write_data,
			dout		=> ram_read_data
		);

	instruction_decoder_inst : entity work.instruction_decoder(rtl)
		port map(
			binary_instruction	=> current_instruction_word,
			decoded_instruction	=> current_instruction
		);

	alu_inst : alu
		port map(
			hold						=> hold,
			program_counter				=> program_counter,
			program_counter_next		=> program_counter_next,
			current_instruction			=> current_instruction,
			source_register_1_data		=> source_register_1_data,
			source_register_2_data		=> source_register_2_data,
			register_write_en			=> register_write_en,
			destination_register_data	=> destination_register_data,
			ram_write_en				=> ram_write_en,
			ram_write_address			=> ram_write_address,
			ram_read_address			=> ram_read_address,
			ram_write_data				=> ram_write_data,
			ram_read_data				=> ram_read_data,
			test_end					=> test_output.test_end
		);

	pc_seq : process (reset_unit_vector, clk, program_counter_next)
	begin
		if reset_unit_vector.program_counter = '1' then
			program_counter <= 0;
		elsif rising_edge(clk) then
			program_counter <= program_counter_next;
		end if;
	end process;

	test_output.program_counter				<= program_counter;
	test_output.instruction					<= current_instruction;
	test_output.source_register_1_data		<= source_register_1_data;
	test_output.source_register_2_data		<= source_register_2_data;
	test_output.destination_register_data	<= destination_register_data;
	test_output.ram_write_data				<= ram_write_data;
	test_output.ram_read_data				<= ram_read_data;
end architecture structural;
