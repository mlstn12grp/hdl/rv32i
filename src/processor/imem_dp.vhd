--------------------------------------------------------------------------------
--! @file imem_dp.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cpu_defs_pkg.all;
use work.cpu_memory_pkg.all;

entity imem_dp is
	generic (
		INIT_DATA	: instruction_memory_binary_t	:= (others => (others => '0'))
	);
	port (
		addra		: in  natural range instruction_memory_binary_t'range;
		write_ena	: in  std_logic;
		clka		: in  std_logic;
		dina		: in  instruction_word_t;
		douta		: out instruction_word_t;
		------------------------------------------------------------------------
		addrb		: in  natural range instruction_memory_binary_t'range;
		write_enb	: in  std_logic;
		clkb		: in  std_logic;
		dinb		: in  instruction_word_t;
		doutb		: out instruction_word_t
	);
end imem_dp;

architecture rtl of imem_dp is
	signal mem : instruction_memory_binary_t	:= 	INIT_DATA;
	attribute syn_ramstyle: string;
	attribute syn_ramstyle of mem: signal is "no_rw_check";
begin
	process (clka, clkb) -- Using port a.
	begin
		if (clka'event and clka = '1') then
			if (write_ena = '1') then
				mem(addra) <= dina;
				-- Using address bus a.
				end if;
			douta <= mem(addra);
		end if;

		if (clkb'event and clkb = '1') then
			if (write_enb = '1') then
				mem(addrb) <= dinb;
				-- Using address bus b.
				end if;
			doutb <= mem(addrb);
		end if;
	end process;
end rtl;
