library ieee;
use ieee.std_logic_1164.all;

use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;
use work.cpu_register_pkg.all;
use work.cpu_memory_pkg.all;

package cpu_test_pkg is

  type test_output_t is record
	program_counter : program_counter_t;
	register_file : register_file_t;
	-- large simulated data memories are problematic as they must be held
	-- in RAM.
	data_memory : data_memory_t;
	instruction : instruction_t;

	source_register_1_data		: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	source_register_2_data		: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	destination_register_data	: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	ram_write_data				: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	ram_read_data				: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);

	test_end    : boolean;
  end record test_output_t;

end cpu_test_pkg;
