--------------------------------------------------------------------------------
--! @file cpu_types_pkg.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.riscv_isa_pkg.all;
use work.cpu_defs_pkg.all;

package cpu_types_pkg is

  subtype register_index_t is natural range 0 to (REGISTER_FILE_SIZE - 1);

  subtype program_counter_t is natural range 0 to ((PROGRAM_MEMORY_SIZE - 1) * 4);

  -- abstract representation of all elements that make up a complete
  -- instruction.
  -- present elements depend on instruction type
  -- * opcode
  -- * destination register
  -- * source registers
  -- * immediate operand
  type instruction_t is record
	  operation             : operation_t;
	  destination_register  : register_index_t;
	  source_register_1     : register_index_t;
	  source_register_2     : register_index_t;
	  immediate_operand     : integer;
  end record instruction_t;

	type reset_vector_t is record
		global			: std_logic;
		firmware_loader	: std_logic;
	end record;

	type reset_unit_vector_t is record
		register_file		: std_logic;
		program_counter		: std_logic;
	end record;

end cpu_types_pkg;
