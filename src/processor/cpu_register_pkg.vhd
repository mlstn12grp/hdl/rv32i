library ieee;
use ieee.std_logic_1164.all;

use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;

package cpu_register_pkg is

  -- type for an abstract register file representation
  -- the RV32I ISA specifies a register file of 32 integer registers
  type register_file_t is array(register_index_t) of integer;

  type register_file_bin_t is array(register_index_t) of std_ulogic_vector((DATA_WORD_WIDTH - 1) downto 0);

end cpu_register_pkg;
