package cpu_defs_pkg is

	constant INSTRUCTION_WORD_WIDTH	: positive := 32;
	constant DATA_WORD_WIDTH        : positive := 32;

	constant REGISTER_FILE_SIZE : positive := 32;

	constant PROGRAM_MEMORY_ADDRESS_WIDTH	: positive	:= 8;
	constant PROGRAM_MEMORY_SIZE			: positive := 2**PROGRAM_MEMORY_ADDRESS_WIDTH;

	constant RAM_ADDRESS_WIDTH	: positive	:= 6;
	constant RAM_SIZE			: positive	:= 2**RAM_ADDRESS_WIDTH;

end cpu_defs_pkg;
