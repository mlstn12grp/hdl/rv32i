package riscv_isa_pkg is
  -- specify all instruction operations
  type operation_t is (
      unknown,
      -- arithmetic
      --   arithmetic with register operands
      op_add,
      op_sub,
      op_sll,
      op_srl,
      op_sra,
      op_and,
      op_or,
      op_xor,
      op_slt,   -- set if less than
      op_sltu,  -- set if less than unsigned
      --   arithmetic with one immediate operand
      op_addi,
      op_slli,
      op_srli,
      op_srai,
      op_andi,
      op_ori,
      op_xori,
      op_slti,
      op_sltiu,
      -- loads
      op_lb,
      op_lbu,
      op_lh,
      op_lhu,
      op_lw,
      -- stores
      op_sb,
      op_sh,
      op_sw,
      -- fences
      op_fence,
      op_fenceI,
      -- branches
      op_beq,
      op_bne,
      op_blt,
      op_bltu,
      op_bge,
      op_bgeu,
      -- other
      op_lui,
      op_auipc,
      op_jal,
      op_jalr,
      -- system
      op_scall_sbreak,
      op_csrrw,
      op_csrrc,
      op_csrrs,
      op_csrrwi,
      op_csrrci,
      op_csrrsi
  );

  -- specify the opcode types
  -- may be used for hierarchical decision trees or more efficient logic
  -- implementation
  type operation_type_t is (
      unknown,
      loads,
      stores,
      branches,
      JALR,
      fences,
      JAL,
      arithmetic, -- remove
      arithmetic_imm,
      arithmetic_reg,
      system,
      AUIPC,
      LUI
      -- all types except of RV32I still missing
  );

  -- the following type will allow querying of the opcode type when opcode
  -- is known.
  type operation_type_map_t is array(operation_t) of operation_type_t;

  -- opcode[6..5] by opcode[4..2]
  type opcode_map_t is array(0 to 3, 0 to 7) of operation_type_t;

  -- specify the instruction types/formats
  -- may be used for hierarchical decision trees or more efficient logic
  -- implementation
  type instruction_type_t is (
      unknown,
      R_type,
      I_type,
      S_type,
      SB_type,
      U_type,
      UJ_type
  );

  -- the following type will allow querying of the instruction type when
  -- instruction/opcode is known
  type instruction_type_map_t is array(operation_t) of instruction_type_t;

  -- operation type by funct3
  type instruction_format_map_t is array(operation_type_t, 0 to 7) of operation_t;

  -- funct3 by funct7 for arithmetic operations
  type arithmetic_format_map_t is array(arithmetic_imm to arithmetic_reg, 0 to 7, 0 to (2**7 - 1)) of operation_t;

  -- Enumeration type listing all RISC-V base and standard extensions
  type extension_t is (
    unknown,
    RV32I,
    RV32M,
    RV32A,
    RV32F,
    RV32C,
    RV64I,
    RV64M,
    RV64A,
    RV64F,
    RV64D,
    RV64C,
    RV128I
  );

  -- Map instruction operations to the RISC-V extension they belong to
  type extension_map_t is array(operation_t) of extension_t;

  ------------------------------------------------------------------------------

  constant operation_type_map : operation_type_map_t := (
      unknown         => unknown,
      op_add          => arithmetic_reg,
      op_sub          => arithmetic_reg,
      op_sll          => arithmetic_reg,
      op_srl          => arithmetic_reg,
      op_sra          => arithmetic_reg,
      op_and          => arithmetic_reg,
      op_or           => arithmetic_reg,
      op_xor          => arithmetic_reg,
      op_slt          => arithmetic_reg,
      op_sltu         => arithmetic_reg,
      op_addi         => arithmetic_imm,
      op_slli         => arithmetic_imm,
      op_srli         => arithmetic_imm,
      op_srai         => arithmetic_imm,
      op_andi         => arithmetic_imm,
      op_ori          => arithmetic_imm,
      op_xori         => arithmetic_imm,
      op_slti         => arithmetic_imm,
      op_sltiu        => arithmetic_imm,
      op_lb           => loads,
      op_lbu          => loads,
      op_lh           => loads,
      op_lhu          => loads,
      op_lw           => loads,
      op_sb           => stores,
      op_sh           => stores,
      op_sw           => stores,
      op_fence        => fences,
      op_fenceI       => fences,
      op_beq          => branches,
      op_bne          => branches,
      op_blt          => branches,
      op_bltu         => branches,
      op_bge          => branches,
      op_bgeu         => branches,
      op_lui          => LUI,
      op_auipc        => AUIPC,
      op_jal          => JAL,
      op_jalr         => JALR,
      op_scall_sbreak => system,
      op_csrrw        => system,
      op_csrrc        => system,
      op_csrrs        => system,
      op_csrrwi       => system,
      op_csrrci       => system,
      op_csrrsi       => system
  );

  constant opcode_map : opcode_map_t := (
    0 => (loads,    unknown,  unknown, fences,  arithmetic_imm, AUIPC,    unknown, unknown),
    1 => (stores,   unknown,  unknown, unknown, arithmetic_reg, LUI,      unknown, unknown),
    2 => (unknown,  unknown,  unknown, unknown, unknown,        unknown,  unknown, unknown),
    3 => (branches, JALR,     unknown, JAL,     system,         unknown,  unknown, unknown)
  );

  constant instruction_type_map : instruction_type_map_t := (
      unknown         => unknown,
      op_add          => R_type,
      op_sub          => R_type,
      op_sll          => R_type,
      op_srl          => R_type,
      op_sra          => R_type,
      op_and          => R_type,
      op_or           => R_type,
      op_xor          => R_type,
      op_slt          => R_type,
      op_sltu         => R_type,
      op_addi         => I_type,
      op_slli         => I_type, -- truncate immediate operand to 5 digits in ALU
      op_srli         => I_type, -- truncate immediate operand to 5 digits in ALU
      op_srai         => I_type, -- truncate immediate operand to 5 digits in ALU
      op_andi         => I_type,
      op_ori          => I_type,
      op_xori         => I_type,
      op_slti         => I_type,
      op_sltiu        => I_type,
      op_lb           => I_type,
      op_lbu          => I_type,
      op_lh           => I_type,
      op_lhu          => I_type,
      op_lw           => I_type,
      op_sb           => S_type,
      op_sh           => S_type,
      op_sw           => S_type,
      op_fence        => I_type,
      op_fenceI       => I_type,
      op_beq          => SB_type,
      op_bne          => SB_type,
      op_blt          => SB_type,
      op_bltu         => SB_type,
      op_bge          => SB_type,
      op_bgeu         => SB_type,
      op_lui          => U_type,
      op_auipc        => U_type,
      op_jal          => UJ_type,
      op_jalr         => I_type,
      op_scall_sbreak => I_type,
      op_csrrw        => I_type,
      op_csrrc        => I_type,
      op_csrrs        => I_type,
      op_csrrwi       => I_type,
      op_csrrci       => I_type,
      op_csrrsi       => I_type
  );

  constant instruction_format_map : instruction_format_map_t := (
    unknown         => (                                                                                                                                  others => unknown),
    loads           => (0 => op_lb,           1 => op_lh,     2 => op_lw,                   4 => op_lbu, 5 => op_lhu,                                     others => unknown),
    stores          => (0 => op_sb,           1 => op_sh,     2 => op_sw,                                                                                 others => unknown),
    branches        => (0 => op_beq,          1 => op_bne,                                  4 => op_blt, 5 => op_bge,     6 => op_bltu, 7 => op_bgeu,     others => unknown),
    JALR            => (                                                                                                                                  others => op_jalr),
    fences          => (0 => op_fence,        1 => op_fenceI,                                                                                             others => unknown),
    JAL             => (                                                                                                                                  others => op_jal),
    arithmetic      => (                                                                                                                                  others => unknown), -- remove
    arithmetic_imm  => (                                                                                                                                  others => unknown),
    arithmetic_reg  => (                                                                                                                                  others => unknown),
    system          => (0 => op_scall_sbreak, 1 => op_csrrw, 2 => op_csrrc, 3 => op_csrrs,               5 => op_csrrwi,  6 => op_csrrci, 7 => op_csrrsi, others => unknown),
    AUIPC           => (                                                                                                                                  others => op_auipc),
    LUI             => (                                                                                                                                  others => op_lui)
  );

  constant arithmetic_format_map : arithmetic_format_map_t := (
    arithmetic_imm => (
      0 => (                              others => op_addi),
      1 => (0 => op_slli,                 others => unknown),
      2 => (                              others => op_slti),
      3 => (                              others => op_sltiu),
      4 => (                              others => op_xori),
      5 => (0 => op_srli, 32 => op_srai,  others => unknown),
      6 => (                              others => op_ori),
      7 => (                              others => op_andi)
    ),
    arithmetic_reg => (
      0 => (0 => op_add, 32 => op_sub,  others => unknown),
      1 => (0 => op_sll,                others => unknown),
      2 => (0 => op_slt,                others => unknown),
      3 => (0 => op_sltu,               others => unknown),
      4 => (0 => op_xor,                others => unknown),
      5 => (0 => op_srl, 32 => op_sra,  others => unknown),
      6 => (0 => op_or,                 others => unknown),
      7 => (0 => op_and,                others => unknown)
    )
  );

  constant extension_map : extension_map_t := (
    unknown         => unknown,
    op_add          => RV32I,
    op_sub          => RV32I,
    op_sll          => RV32I,
    op_srl          => RV32I,
    op_sra          => RV32I,
    op_and          => RV32I,
    op_or           => RV32I,
    op_xor          => RV32I,
    op_slt          => RV32I,
    op_sltu         => RV32I,
    op_addi         => RV32I,
    op_slli         => RV32I,
    op_srli         => RV32I,
    op_srai         => RV32I,
    op_andi         => RV32I,
    op_ori          => RV32I,
    op_xori         => RV32I,
    op_slti         => RV32I,
    op_sltiu        => RV32I,
    op_lb           => RV32I,
    op_lbu          => RV32I,
    op_lh           => RV32I,
    op_lhu          => RV32I,
    op_lw           => RV32I,
    op_sb           => RV32I,
    op_sh           => RV32I,
    op_sw           => RV32I,
    op_fence        => RV32I,
    op_fenceI       => RV32I,
    op_beq          => RV32I,
    op_bne          => RV32I,
    op_blt          => RV32I,
    op_bltu         => RV32I,
    op_bge          => RV32I,
    op_bgeu         => RV32I,
    op_lui          => RV32I,
    op_auipc        => RV32I,
    op_jal          => RV32I,
    op_jalr         => RV32I,
    op_scall_sbreak => RV32I,
    op_csrrw        => RV32I,
    op_csrrc        => RV32I,
    op_csrrs        => RV32I,
    op_csrrwi       => RV32I,
    op_csrrci       => RV32I,
    op_csrrsi       => RV32I
  );

end riscv_isa_pkg;
