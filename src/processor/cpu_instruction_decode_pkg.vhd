library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_isa_pkg.all;
use work.cpu_types_pkg.all;
use work.cpu_memory_pkg.all;
use work.cpu_utils_pkg.all;

package cpu_instruction_decode_pkg is

  function get_operation_type (binary_instruction : in instruction_word_t) return operation_type_t;

  function get_operation (binary_instruction : in instruction_word_t) return operation_t;

  function decode (binary_instruction : in instruction_word_t) return instruction_t;

  function decode_all (binary_instruction_memory : in instruction_memory_binary_t) return instruction_memory_record_t;

  impure function import_srec (filepath : in string; debug : in boolean := false) return instruction_memory_record_t;

end cpu_instruction_decode_pkg;

package body cpu_instruction_decode_pkg is

  -- TODO: debug prints
  function get_operation_type (binary_instruction : in instruction_word_t) return operation_type_t is
    variable op_type    : operation_type_t;
    variable index_6_5  : natural;
    variable index_4_2  : natural;
  begin
    index_6_5 := to_integer(unsigned(binary_instruction(6 downto 5)));
    index_4_2 := to_integer(unsigned(binary_instruction(4 downto 2)));

    if (binary_instruction(1 downto 0) = "11") then
      op_type := opcode_map(index_6_5, index_4_2);
    else -- implement compressed ISA here later
      op_type := unknown;
    end if;

    return op_type;
  end get_operation_type;

  -- TODO: debug prints
  function get_operation (binary_instruction : in instruction_word_t) return operation_t is
    variable op_type  : operation_type_t;
    variable op       : operation_t;
    variable funct3_i : natural;
    variable funct7_i : natural;
  begin
    op_type := get_operation_type (binary_instruction => binary_instruction);
    report "operation type: " & op_type'subtype'image(op_type)
      severity note;

    case op_type is
      when LUI | AUIPC | JAL | JALR =>
        op := instruction_format_map(op_type, 0);
      when branches | loads | stores | fences | system =>
        funct3_i := to_integer(unsigned(binary_instruction(14 downto 12)));
        op := instruction_format_map(op_type, funct3_i);
      when arithmetic_reg | arithmetic_imm =>
        funct3_i := to_integer(unsigned(binary_instruction(14 downto 12)));
        funct7_i := to_integer(unsigned(binary_instruction(31 downto 25)));
        op := arithmetic_format_map(op_type, funct3_i, funct7_i);
      when others =>
        op := unknown;
    end case;

    return op;
  end get_operation;

  -- TODO: debug prints
  function decode (binary_instruction : in instruction_word_t) return instruction_t is
    variable instruction_record   : instruction_t;
    variable composite_immediate  : std_ulogic_vector(31 downto 0);
  begin
    instruction_record.operation := get_operation (binary_instruction => binary_instruction);

    case instruction_type_map(instruction_record.operation) is
      when R_type =>
        instruction_record.destination_register := to_integer(unsigned(binary_instruction(11 downto 7)));
        instruction_record.source_register_1    := to_integer(unsigned(binary_instruction(19 downto 15)));
        instruction_record.source_register_2    := to_integer(unsigned(binary_instruction(24 downto 20)));
      when I_type =>
        instruction_record.destination_register := to_integer(unsigned(binary_instruction(11 downto 7)));
        instruction_record.source_register_1    := to_integer(unsigned(binary_instruction(19 downto 15)));
        instruction_record.immediate_operand    := to_integer(signed(binary_instruction(31 downto 20)));
      when S_type =>
        instruction_record.source_register_1    := to_integer(unsigned(binary_instruction(19 downto 15)));
        instruction_record.source_register_2    := to_integer(unsigned(binary_instruction(24 downto 20)));
        composite_immediate(11 downto 0)        := binary_instruction(31 downto 25) & binary_instruction(11 downto 7);
        instruction_record.immediate_operand    := to_integer(signed(composite_immediate(11 downto 0)));
      when SB_type =>
        instruction_record.source_register_1    := to_integer(unsigned(binary_instruction(19 downto 15)));
        instruction_record.source_register_2    := to_integer(unsigned(binary_instruction(24 downto 20)));
        composite_immediate(12 downto 0)        := binary_instruction(31 downto 31) & binary_instruction(7 downto 7) & binary_instruction(30 downto 25) & binary_instruction(11 downto 8) & "0";
        instruction_record.immediate_operand    := to_integer(signed(composite_immediate(12 downto 0)));
      when U_type =>
        instruction_record.destination_register := to_integer(unsigned(binary_instruction(11 downto 7)));
        composite_immediate(19 downto 0)        := binary_instruction(31 downto 12);
        instruction_record.immediate_operand    := to_integer(signed(composite_immediate(19 downto 0)));
      when UJ_type =>
        instruction_record.destination_register := to_integer(unsigned(binary_instruction(11 downto 7)));
        composite_immediate(20 downto 0)        := binary_instruction(31 downto 31) & binary_instruction(19 downto 12) & binary_instruction(20 downto 20) & binary_instruction(30 downto 21) & "0";
        instruction_record.immediate_operand    := to_integer(signed(composite_immediate(20 downto 0)));
      when others =>
        null; -- report error about unknown instruction format
    end case;

    -- pragma translate_off
    report "decoded: " & operation_t'image(instruction_record.operation) & " " & integer'image(instruction_record.destination_register) & ", " & integer'image(instruction_record.source_register_1) & ", " & integer'image(instruction_record.source_register_2) & ", " & integer'image(instruction_record.immediate_operand);
    -- pragma translate_on

    return instruction_record;
  end decode;

  -- TODO: debug prints
  function decode_all (binary_instruction_memory : in instruction_memory_binary_t) return instruction_memory_record_t is
    variable imem_rec : instruction_memory_record_t;
  begin
    for i in binary_instruction_memory'range loop
      imem_rec(i) := decode(binary_instruction_memory(i));
    end loop;

    return imem_rec;
  end decode_all;

  -- TODO: debug prints
  impure function import_srec (filepath : in string; debug : in boolean := false) return instruction_memory_record_t is
    variable imem_bin : instruction_memory_binary_t;
  begin
    imem_bin := parse_srecord(filepath => filepath, debug => debug);

    return decode_all(imem_bin);
  end import_srec;

end cpu_instruction_decode_pkg;
