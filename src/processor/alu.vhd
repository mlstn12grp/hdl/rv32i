--------------------------------------------------------------------------------
--! @file alu.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------
--! @todo Add more comprehensive logging and a log level generic from VUnit
--
--! @todo Implement missing load and store instructions
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.riscv_isa_pkg.all;
use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;

entity alu is
	port (
		hold						: in  std_logic;
		program_counter				: in  program_counter_t;
		program_counter_next		: out program_counter_t;
		current_instruction			: in  instruction_t;
		source_register_1_data		: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
		source_register_2_data		: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
		register_write_en			: out std_logic;
		destination_register_data	: out std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
		ram_write_en				: out std_logic;
		ram_write_address			: out std_logic_vector((RAM_ADDRESS_WIDTH - 1) downto 0);
		ram_read_address			: out std_logic_vector((RAM_ADDRESS_WIDTH - 1) downto 0);
		ram_write_data				: out std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
		ram_read_data				: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
		test_end					: out boolean
	);
end entity;

architecture logic of alu is

	type less_than_operation_t is (
		LESS_THAN_SIGNED,
		LESS_THAN_UNSIGNED
	);
	type logic_operation_t is (
		LOGIC_AND,
		LOGIC_OR,
		LOGIC_XOR
	);
	type shift_operation_t is (
		SHIFT_SLL,
		SHIFT_SRL,
		SHIFT_SRA
	);

	signal add_operand_1				: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal add_operand_2				: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal add_result					: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal less_than_operator			: less_than_operation_t;
	signal less_than_operand_1			: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal less_than_operand_2			: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal less_than_result				: boolean;
	signal shift_operator				: shift_operation_t;
	signal shift_operand				: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal shift_amount					: natural range 0 to (DATA_WORD_WIDTH - 1);
	signal shift_result					: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal logic_operator				: logic_operation_t;
	signal logic_operand_1				: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal logic_operand_2				: signed((DATA_WORD_WIDTH - 1) downto 0);
	signal logic_result					: signed((DATA_WORD_WIDTH - 1) downto 0);

begin

	main_alu_comb : process (
		hold,
		program_counter,
		current_instruction,
		source_register_1_data,
		source_register_2_data,
		ram_read_data,
		add_result,
		less_than_result,
		shift_result,
		logic_result
		)
	begin
		if hold = '0' then
			report "pc: " & program_counter'subtype'image(program_counter);
			case current_instruction.operation is
				----------------------------------------------------------------
				-- arithmetic on registers
				----------------------------------------------------------------
				when op_add =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(add_result);
					add_operand_1				<= signed(source_register_1_data);
					add_operand_2				<= signed(source_register_2_data);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_sub =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(add_result);
					add_operand_1				<= signed(source_register_1_data);
					add_operand_2				<= (- signed(source_register_2_data));
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_sll =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(shift_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= signed(source_register_1_data);
					shift_amount				<= to_integer(unsigned(source_register_2_data(4 downto 0)));
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_srl =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(shift_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SRL;
					shift_operand				<= signed(source_register_1_data);
					shift_amount				<= to_integer(unsigned(source_register_2_data(4 downto 0)));
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_sra =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(shift_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SRA;
					shift_operand				<= signed(source_register_1_data);
					shift_amount				<= to_integer(unsigned(source_register_2_data(4 downto 0)));
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_and =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(logic_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= signed(source_register_1_data);
					logic_operand_2				<= signed(source_register_2_data);
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_or =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(logic_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_OR;
					logic_operand_1				<= signed(source_register_1_data);
					logic_operand_2				<= signed(source_register_2_data);
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_xor =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(logic_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_XOR;
					logic_operand_1				<= signed(source_register_1_data);
					logic_operand_2				<= signed(source_register_2_data);
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_slt =>
					register_write_en			<= '1';
					destination_register_data	<= (0 => '1', others => '0') when less_than_result else (others => '0');
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= source_register_2_data;
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_sltu =>
					register_write_en			<= '1';
					destination_register_data	<= (0 => '1', others => '0') when less_than_result else (others => '0');
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_UNSIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= source_register_2_data;
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				----------------------------------------------------------------
				-- arithmetic with immediate operand
				----------------------------------------------------------------
				when op_addi =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(add_result);
					add_operand_1				<= signed(source_register_1_data);
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_slli =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(shift_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= signed(source_register_1_data);
					report "imm op: " & integer'image(current_instruction.immediate_operand);
					shift_amount				<= to_integer(unsigned(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH)(4 downto 0)));
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_srli =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(shift_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SRL;
					shift_operand				<= signed(source_register_1_data);
					shift_amount				<= to_integer(unsigned(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH)(4 downto 0)));
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_srai =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(shift_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SRA;
					shift_operand				<= signed(source_register_1_data);
					shift_amount				<= to_integer(unsigned(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH)(4 downto 0)));
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_andi =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(logic_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= signed(source_register_1_data);
					logic_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_ori =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(logic_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_OR;
					logic_operand_1				<= signed(source_register_1_data);
					logic_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_xori =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(logic_result);
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_XOR;
					logic_operand_1				<= signed(source_register_1_data);
					logic_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_slti =>
					register_write_en			<= '1';
					destination_register_data	<= (0 => '1', others => '0') when less_than_result else (others => '0');
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= std_logic_vector(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH));
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_sltiu =>
					register_write_en			<= '1';
					destination_register_data	<= (0 => '1', others => '0') when less_than_result else (others => '0');
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_UNSIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= std_logic_vector(to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH));
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				----------------------------------------------------------------
				-- loads and stores
				----------------------------------------------------------------
				when op_lw =>
					report "lw addr: " & to_hstring(signed(source_register_1_data) + to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH));
					register_write_en			<= '1';
					destination_register_data	<= ram_read_data;
					add_operand_1				<= signed(source_register_1_data);
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= std_logic_vector(add_result((RAM_ADDRESS_WIDTH - 1 + 2) downto 2));
					program_counter_next		<= program_counter + 4;
				when op_sw =>
					-- report "sw addr: " & integer'image(integer(to_integer(unsigned(source_register_1_data)) + current_instruction.immediate_operand) / 4);
					report "sw addr: " & to_hstring(signed(source_register_1_data) + to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH));
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(source_register_1_data);
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '1';
					ram_write_address			<= std_logic_vector(add_result((RAM_ADDRESS_WIDTH - 1 + 2) downto 2));
					ram_write_data				<= source_register_2_data;
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
					-- pragma translate_off
					assert (not (add_result = 0 and source_register_2_data = X"DEADBEEF"))
						report "Failure of software test"
							severity failure;
					-- pragma translate_on
				----------------------------------------------------------------
				-- branches
				----------------------------------------------------------------
				when op_beq =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					if source_register_1_data = source_register_2_data then
						program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					else
						program_counter_next		<= program_counter + 4;
					end if;
				when op_bne =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					if not (source_register_1_data = source_register_2_data) then
						program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					else
						program_counter_next		<= program_counter + 4;
					end if;
				when op_blt =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= source_register_2_data;
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					if less_than_result then
						program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					else
						program_counter_next		<= program_counter + 4;
					end if;
				when op_bltu =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_UNSIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= source_register_2_data;
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					if less_than_result then
						program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					else
						program_counter_next		<= program_counter + 4;
					end if;
				when op_bge =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= source_register_2_data;
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					if not less_than_result then
						program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					else
						program_counter_next		<= program_counter + 4;
					end if;
				when op_bgeu =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_UNSIGNED;
					less_than_operand_1			<= source_register_1_data;
					less_than_operand_2			<= source_register_2_data;
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					if not less_than_result then
						program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					else
						program_counter_next		<= program_counter + 4;
					end if;
				----------------------------------------------------------------
				-- special
				----------------------------------------------------------------
				when op_lui =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(to_signed(current_instruction.immediate_operand, 20) & to_signed(0, (DATA_WORD_WIDTH - 20)));
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= program_counter + 4;
				when op_auipc =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(add_result);
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, 20) & to_signed(0, (DATA_WORD_WIDTH - 20));
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= to_integer(unsigned(add_result));
				when op_jal =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(to_unsigned(program_counter + 4, DATA_WORD_WIDTH));
					add_operand_1				<= signed(to_unsigned(program_counter, DATA_WORD_WIDTH));
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					report "op_jal " & to_hstring(add_result);
					program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
					-- pragma translate_off
					-- terminate simulation upon entry of infinite loop
					if current_instruction.immediate_operand = 0 then
						report "This is the end of the test";
						test_end	<= true;
					end if;
					-- pragma translate_on
				when op_jalr =>
					register_write_en			<= '1';
					destination_register_data	<= std_logic_vector(to_unsigned(program_counter + 4, DATA_WORD_WIDTH));
					add_operand_1				<= signed(source_register_1_data);
					add_operand_2				<= to_signed(current_instruction.immediate_operand, DATA_WORD_WIDTH);
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= to_integer(unsigned(add_result((PROGRAM_MEMORY_ADDRESS_WIDTH - 1 + 2) downto 0)));
				when others =>
					register_write_en			<= '0';
					destination_register_data	<= (others => '0');
					add_operand_1				<= (others => '0');
					add_operand_2				<= (others => '0');
					less_than_operator			<= LESS_THAN_SIGNED;
					less_than_operand_1			<= (others => '0');
					less_than_operand_2			<= (others => '0');
					shift_operator				<= SHIFT_SLL;
					shift_operand				<= (others => '0');
					shift_amount				<= 0;
					logic_operator				<= LOGIC_AND;
					logic_operand_1				<= (others => '0');
					logic_operand_2				<= (others => '0');
					ram_write_en				<= '0';
					ram_write_address			<= (others => '0');
					ram_write_data				<= (others => '0');
					ram_read_address			<= (others => '0');
					program_counter_next		<= 0;
			end case;
		else
			register_write_en			<= '0';
			destination_register_data	<= (others => '0');
			add_operand_1				<= (others => '0');
			add_operand_2				<= (others => '0');
			less_than_operator			<= LESS_THAN_SIGNED;
			less_than_operand_1			<= (others => '0');
			less_than_operand_2			<= (others => '0');
			shift_operator				<= SHIFT_SLL;
			shift_operand				<= (others => '0');
			shift_amount				<= 0;
			logic_operator				<= LOGIC_AND;
			logic_operand_1				<= (others => '0');
			logic_operand_2				<= (others => '0');
			ram_write_en				<= '0';
			ram_write_address			<= (others => '0');
			ram_write_data				<= (others => '0');
			ram_read_address			<= (others => '0');
			program_counter_next		<= program_counter;
		end if;
	end process;

	adder_comb : process (add_operand_1, add_operand_2)
	begin
		add_result	<= add_operand_1 + add_operand_2;
	end process;

	less_than_comparison_comb : process (less_than_operator, less_than_operand_1, less_than_operand_2)
	begin
		case less_than_operator is
			when LESS_THAN_SIGNED =>
				less_than_result <= signed(less_than_operand_1) < signed(less_than_operand_2);
			when LESS_THAN_UNSIGNED =>
				less_than_result <= unsigned(less_than_operand_1) < unsigned(less_than_operand_2);
		end case;
	end process;

	shifter_comb : process (shift_operator, shift_operand, shift_amount)
	begin
		case shift_operator is
			when SHIFT_SLL =>
				shift_result	<= shift_operand sll shift_amount;
			when SHIFT_SRL =>
				shift_result	<= shift_operand srl shift_amount;
			when SHIFT_SRA =>
				shift_result	<= shift_operand sra shift_amount;
		end case;
	end process;

	logic_comb : process (logic_operator, logic_operand_1, logic_operand_2)
	begin
		case logic_operator is
			when LOGIC_AND =>
				logic_result	<= logic_operand_1 and logic_operand_2;
			when LOGIC_OR =>
				logic_result	<= logic_operand_1 or logic_operand_2;
			when LOGIC_XOR =>
				logic_result	<= logic_operand_1 xor logic_operand_2;
		end case;
	end process;

end architecture;
