--------------------------------------------------------------------------------
--! @file soc_top.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------
--! @todo Conditional generation of device/manufacturer specific instances,
--! e.g. oscillator/PLL
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cpu_defs_pkg.all;
use work.cpu_types_pkg.all;
use work.cpu_memory_pkg.all;
use work.cpu_test_pkg.all;

entity soc_top is
	port (
		rstn		: in  std_logic;
		uart_rx		: in  std_logic;
		uart_tx		: out std_logic;
		debug_leds	: out std_logic_vector(1 to 8)
	);
end entity;

architecture structural of soc_top is

	component OSCH
		-- synthesis translate_off
		generic (
		NOM_FREQ	: string	:= "2.08"
		);
		-- synthesis translate_on
		port (
		stdby		: in  std_logic;
		osc			: out std_logic;
		sedstdby	: out std_logic
		);
	end component;

	attribute NOM_FREQ	: string;
	attribute NOM_FREQ of OSCH_inst	: label is "2.08";

	component processor
		port (
			clk					: in  std_ulogic;
			rst					: in  reset_vector_t;
			hold				: in  std_ulogic;
			debug_clk			: in  std_ulogic;
			debug_wr_address	: in  std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
			debug_wr_data		: in  instruction_word_t;
			debug_wr_en			: in  std_ulogic;
			test_output			: out test_output_t
		);
	end component;

	component firmware_loader
		port (
			clk						: in  std_logic;
			rst						: in  std_logic;
			uart_rx					: in  std_logic;
			uart_tx					: out std_logic;
			proc_hold				: out std_logic;
			proc_rst				: out std_logic;
			proc_wr_en				: out std_logic;
			instruction_address		: out std_logic_vector(31 downto 0);
			instruction_data		: out std_logic_vector(31 downto 0)
		);
	end component;

	----------------------------------------------------------------------------

	constant SYSTEM_CLOCK_FREQ	: positive	:= 2_080_000;

	constant FIRMWARE_LOADER_BAUD_RATE	: positive	:= 19_200;

	----------------------------------------------------------------------------

	signal rst			: reset_vector_t;
	signal hold			: std_logic;

	signal osc_clk		: std_logic;
	signal clk			: std_logic;

	constant CLK_DIV	: positive	:= (SYSTEM_CLOCK_FREQ / 16);
	signal clk_counter	: natural range 0 to CLK_DIV;
	signal rst_counter	: natural range 0 to 3	:= 0;

	signal data		: std_logic_vector(8 downto 0);
	signal en		: std_logic;
	signal ready	: std_logic;
	signal busy		: std_logic;
	signal done		: std_logic;

	signal debug_wr_address	: std_logic_vector((DATA_WORD_WIDTH - 1) downto 0);
	signal debug_wr_data	: instruction_word_t;
	signal debug_wr_en		: std_ulogic;

	signal test_output	: test_output_t;

	----------------------------------------------------------------------------

	for CPU_inst : processor
		use entity work.cpu (structural)
			generic map (
				SYNTHESIS	=> true
			)
			port map (
				clk					=> clk,
				rst					=> rst,
				hold				=> hold,
				debug_clk			=> debug_clk,
				debug_wr_address	=> debug_wr_address,
				debug_wr_data		=> debug_wr_data,
				debug_wr_en			=> debug_wr_en,
				test_output			=> test_output
			);

	for firmware_loader_inst : firmware_loader
		use entity work.firmware_loader (rtl)
			generic map (
				UART_BAUD_PERIOD	=> (SYSTEM_CLOCK_FREQ / FIRMWARE_LOADER_BAUD_RATE)
			)
			port map (
				clk					=> clk,
				rst					=> rst,
				uart_rx				=> uart_rx,
				uart_tx				=> uart_tx,
				proc_hold			=> proc_hold,
				proc_prog			=> open,
				proc_rst			=> proc_rst,
				proc_wr_en			=> proc_wr_en,
				instruction_address	=> instruction_address,
				instruction_data	=> instruction_data
			);

begin

	OSCH_inst : OSCH
		-- synthesis translate_off
		generic map (
			NOM_FREQ	=> "2.08"
		)
		-- synthesis translate_on
		port map (
			stdby		=> '0',
			osc			=> osc_clk,
			sedstdby	=> open
		);

	CPU_inst : processor
		port map (
			clk					=> clk,
			rst					=> rst,
			hold				=> hold,
			debug_clk			=> osc_clk,
			debug_wr_address	=> debug_wr_address,
			debug_wr_data		=> debug_wr_data,
			debug_wr_en			=> debug_wr_en,
			test_output			=> test_output
		);

	firmware_loader_inst : firmware_loader
		port map (
			clk					=> osc_clk,
			rst					=> rst.global,
			uart_rx				=> uart_rx,
			uart_tx				=> uart_tx,
			proc_hold			=> hold,
			proc_rst			=> rst.firmware_loader,
			proc_wr_en			=> debug_wr_en,
			instruction_address	=> debug_wr_address,
			instruction_data	=> debug_wr_data
		);

	reset_pulse: process (clk, rstn, rst)
	begin
		if rstn = '0' then
			rst_counter <= rst_counter'subtype'low;
			rst.global	<= '1';
		elsif rising_edge(clk) then
			if rst_counter = rst_counter'subtype'high then
				rst_counter <= rst_counter;
				rst.global	<= '0';
			else
				rst_counter	<= rst_counter + 1;
				rst.global	<= '1';
			end if;
		end if;
	end process;

	clk_divider_seq : process (osc_clk, rstn, rst)
	begin
		if rstn = '0' or rst.firmware_loader = '1' then
			clk_counter <= clk_counter'subtype'low;
			clk <= '0';
		elsif rising_edge(osc_clk) then
			if clk_counter = clk_counter'subtype'high then
				clk_counter	<= clk_counter'subtype'low;
				clk <= not clk;
			else
				clk_counter <= clk_counter + 1;
				clk <= clk;
			end if;
		end if;
	end process;

	-- debug_leds <= (1 => rst, 2 => clk, others => '1');
	debug_leds <= not std_logic_vector(to_unsigned(test_output.program_counter, debug_leds'length+2)(debug_leds'length+2-1 downto 2));

end architecture;
