--------------------------------------------------------------------------------
--! @file uart_rx.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity uart_rx is
	generic (
		MAX_DATA_LENGTH		: natural range 9 to 9	:= 9;
		MAX_BAUD_PERIOD		: positive	:= 1024;
		SYNCHRONIZER_LENGTH	: positive range 2 to positive'high	:= 2
	);
	port (
		clk			: in  std_logic;
		rst			: in  std_logic;
		data_out	: out std_logic_vector((MAX_DATA_LENGTH - 1) downto 0);
		clear		: in  std_logic;
		ready		: out std_logic;
		err_parity	: out std_logic;
		err_frame	: out std_logic;
		busy		: out std_logic;
		done		: out std_logic;
		rx			: in  std_logic;
		baud_period	: in  natural range 0 to MAX_BAUD_PERIOD;
		data_length	: in  natural range 7 to MAX_DATA_LENGTH;
		stop_bits	: in  natural range 1 to 2;
		parity		: in  boolean;
		parity_odd	: in  boolean
	);
end entity;

architecture rtl of uart_rx is
	type state_t is (
		STATE_IDLE,
		STATE_CENTER_START_BIT,
		STATE_START_BIT_CLEAR_CLK_COUNTER,
		STATE_START_BIT,
		STATE_REGISTER_DATA_BIT,
		STATE_DATA_BITS,
		STATE_CHECK_PARITY_BIT,
		STATE_SET_PARITY_ERROR,
		STATE_PARITY_BIT,
		STATE_STOP_BITS_CLEAR_BIT_COUNTER,
		STATE_STOP_BITS,
		STATE_SET_FRAME_ERROR,
		STATE_DONE
	);
	signal state		: state_t;
	signal next_state	: state_t;

	signal clk_counter		: baud_period'subtype;
	signal clk_counter_rst	: std_logic;
	signal clk_counter_en	: std_logic;
	signal bit_counter		: natural range 0 to MAX_DATA_LENGTH;
	signal bit_counter_rst	: std_logic;
	signal bit_counter_en	: std_logic;

	signal synchronous_rx	: std_logic_vector(1 to SYNCHRONIZER_LENGTH);

	signal data				: data_out'subtype;
begin

	input_synchronizer : process(clk, rst)
	begin
		if rst = '1' then
			synchronous_rx	<= (others => '0');
		elsif rising_edge(clk) then
			synchronous_rx	<= synchronous_rx(synchronous_rx'left + 1 to synchronous_rx'right) & rx;
		end if;
	end process;

	----------------------------------------------------------------------------

	state_transition_comb : process (
		state,
		synchronous_rx,
		clk_counter,
		bit_counter,
		baud_period,
		parity,
		parity_odd,
		data,
		data_length,
		stop_bits
		)
	begin
		case state is
			when STATE_IDLE =>
				if synchronous_rx(synchronous_rx'left to (synchronous_rx'left + 1)) = "10" then
					next_state	<= STATE_CENTER_START_BIT;
				else
					next_state	<= STATE_IDLE;
				end if;
			when STATE_CENTER_START_BIT =>
				if clk_counter = ((baud_period / 2) - 1) then
					if synchronous_rx(synchronous_rx'left) = '0' then
						next_state	<= STATE_START_BIT_CLEAR_CLK_COUNTER;
					else
						next_state	<= STATE_SET_FRAME_ERROR;
					end if;
				else
					next_state	<= STATE_CENTER_START_BIT;
				end if;
			when STATE_START_BIT_CLEAR_CLK_COUNTER =>
				next_state	<= STATE_START_BIT;
			when STATE_START_BIT =>
				if clk_counter = baud_period then
					next_state	<= STATE_REGISTER_DATA_BIT;
				else
					next_state	<= STATE_START_BIT;
				end if;
			when STATE_REGISTER_DATA_BIT =>
				next_state 	<= STATE_DATA_BITS;
			when STATE_DATA_BITS =>
				if clk_counter = 0 then
					if bit_counter = data_length then
						if parity then
							next_state	<= STATE_CHECK_PARITY_BIT;
						else
							next_state	<= STATE_STOP_BITS_CLEAR_BIT_COUNTER;
						end if;
					else
						next_state	<= STATE_REGISTER_DATA_BIT;
					end if;
				else
					next_state	<= STATE_DATA_BITS;
				end if;
			when STATE_CHECK_PARITY_BIT =>
				if parity_odd then
					if not (synchronous_rx(synchronous_rx'left) = xor data((data_length - 1) downto 0)) then
						next_state	<= STATE_PARITY_BIT;
					else
						next_state	<= STATE_SET_PARITY_ERROR;
					end if;
				else
					if synchronous_rx(synchronous_rx'left) = xor data((data_length - 1) downto 0) then
						next_state	<= STATE_PARITY_BIT;
					else
						next_state	<= STATE_SET_PARITY_ERROR;
					end if;
				end if;
			when STATE_SET_PARITY_ERROR =>
				next_state	<= STATE_IDLE;
			when STATE_PARITY_BIT =>
				if clk_counter = baud_period then
					next_state	<= STATE_STOP_BITS;
				else
					next_state	<= STATE_PARITY_BIT;
				end if;
			when STATE_STOP_BITS_CLEAR_BIT_COUNTER =>
				next_state	<= STATE_STOP_BITS;
			when STATE_STOP_BITS =>
				if synchronous_rx(synchronous_rx'left) = '0' then
					next_state	<= STATE_SET_FRAME_ERROR;
				elsif bit_counter = (stop_bits - 1) then
					next_state	<= STATE_DONE;
				else
					next_state	<= STATE_STOP_BITS;
				end if;
			when STATE_SET_FRAME_ERROR =>
				next_state	<= STATE_IDLE;
			when STATE_DONE =>
				next_state	<= STATE_IDLE;
		end case;
	end process;

	state_transition_seq : process (clk, rst)
	begin
		if rst = '1' then
			state <= STATE_IDLE;
		elsif rising_edge(clk) then
			state <= next_state;
		end if;
	end process;

	output_seq : process (clk, rst, next_state)
	begin
		if rst = '1' then
			busy			<= '0';
			done			<= '0';
			data			<= (others => '0');
			clk_counter_rst	<= '0';
			clk_counter_en	<= '0';
			bit_counter_rst	<= '0';
			bit_counter_en	<= '0';
		elsif rising_edge(clk) then
			case next_state is
				when STATE_IDLE =>
					busy			<= '0';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_CENTER_START_BIT =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_START_BIT_CLEAR_CLK_COUNTER =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_START_BIT =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '0';
				when STATE_REGISTER_DATA_BIT =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					data(bit_counter)	<= synchronous_rx(synchronous_rx'left);
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '1';
				when STATE_DATA_BITS =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '1';
				when STATE_CHECK_PARITY_BIT =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_SET_PARITY_ERROR =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_PARITY_BIT =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_STOP_BITS_CLEAR_BIT_COUNTER =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_STOP_BITS =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '1';
				when STATE_SET_FRAME_ERROR =>
					busy			<= '1';
					done			<= '0';
					data			<= data;
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_DONE =>
					busy			<= '0';
					done			<= '1';
					data			<= data;
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
			end case;
		end if;
	end process;

	----------------------------------------------------------------------------

	user_signals_seq : process (clk, rst)
	begin
		if rst = '1' then
			ready		<= '0';
			err_parity	<= '0';
			err_frame	<= '0';
			data_out	<= (others => '0');
		elsif rising_edge(clk) then
			if clear = '1' then
				ready		<= '0';
				err_parity	<= '0';
				err_frame	<= '0';
				data_out	<= (others => '0');
			else
				case state is
					when STATE_DONE =>
						ready		<= '1';
						err_parity	<= err_parity;
						err_frame	<= err_frame;
						data_out	<= data;
					when STATE_SET_PARITY_ERROR =>
						ready		<= ready;
						err_parity	<= '1';
						err_frame	<= err_frame;
						data_out	<= data_out;
					when STATE_SET_FRAME_ERROR =>
						ready		<= ready;
						err_parity	<= err_parity;
						err_frame	<= '1';
						data_out	<= data_out;
					when others =>
						ready		<= ready;
						err_parity	<= err_parity;
						err_frame	<= err_frame;
						data_out	<= data_out;
				end case;
			end if;
		end if;
	end process;

	----------------------------------------------------------------------------

	clk_counter_seq : process (
		clk,
		rst,
		clk_counter_rst,
		clk_counter_en
		)
	begin
		if rst = '1' then
			clk_counter <= 0;
		elsif rising_edge(clk) then
			if clk_counter_rst = '1' or clk_counter = baud_period then
				clk_counter <= clk_counter'subtype'low;
			elsif clk_counter_en = '1' then
				clk_counter <= clk_counter + 1;
			end if;
		end if;
	end process;
	assert (clk_counter <= baud_period)
		report "clk_counter went out of range baud_period"
			severity failure;

	bit_counter_seq : process (
		clk,
		rst,
		bit_counter_rst,
		bit_counter_en,
		clk_counter
		)
	begin
		if rst = '1' then
			bit_counter <= 0;
		elsif rising_edge(clk) then
			if bit_counter_rst = '1' then
				bit_counter <= bit_counter'subtype'low;
			elsif bit_counter_en = '1' and clk_counter = baud_period then
				if bit_counter = bit_counter'subtype'high then
					bit_counter <= bit_counter'subtype'low;
				else
					bit_counter <= bit_counter + 1;
				end if;
			end if;
		end if;
	end process;

end architecture;
