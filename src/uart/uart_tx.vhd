--------------------------------------------------------------------------------
--! @file uart_tx.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity uart_tx is
	generic (
		MAX_DATA_LENGTH	: natural range 9 to 9	:= 9;
		MAX_BAUD_PERIOD	: positive	:= 1024
	);
	port (
		clk			: in  std_logic;
		rst			: in  std_logic;
		data_in		: in  std_logic_vector((MAX_DATA_LENGTH - 1) downto 0);
		en			: in  std_logic;
		ready		: out std_logic;
		busy		: out std_logic;
		done		: out std_logic;
		tx			: out std_logic;
		baud_period	: in  natural range 0 to MAX_BAUD_PERIOD;
		data_length	: in  natural range 7 to MAX_DATA_LENGTH;
		stop_bits	: in  natural range 1 to 2;
		parity		: in  boolean;
		parity_odd	: in  boolean
	);
end entity;

architecture rtl of uart_tx is
	type state_t is (
		STATE_IDLE,
		STATE_START_BIT,
		STATE_DATA_BITS,
		STATE_PARITY_BIT,
		STATE_STOP_BITS_CLEAR_BIT_COUNTER,
		STATE_STOP_BITS,
		STATE_DONE
	);
	signal state		: state_t;
	signal next_state	: state_t;

	signal clk_counter		: baud_period'subtype;
	signal clk_counter_rst	: std_logic;
	signal clk_counter_en	: std_logic;
	signal bit_counter		: natural range 0 to MAX_DATA_LENGTH;
	signal bit_counter_rst	: std_logic;
	signal bit_counter_en	: std_logic;

	signal data				: data_in'subtype;
begin

	state_transition_comb : process (
		state,
		en,
		clk_counter,
		bit_counter
		)
	begin
		case state is
			when STATE_IDLE =>
				if en = '1' then
					next_state <= STATE_START_BIT;
				else
					next_state <= STATE_IDLE;
				end if;
			when STATE_START_BIT =>
				if clk_counter = baud_period then
					next_state <= STATE_DATA_BITS;
				else
					next_state <= STATE_START_BIT;
				end if;
			when STATE_DATA_BITS =>
				if bit_counter = data_length then
					if PARITY then
						next_state <= STATE_PARITY_BIT;
					else
						next_state <= STATE_STOP_BITS_CLEAR_BIT_COUNTER;
					end if;
				else
					next_state <= STATE_DATA_BITS;
				end if;
				-- pragma translate_off
				assert (bit_counter <= data_length)
					report "bit_counter beyond data length"
						severity failure;
				-- pragma translate_on
			when STATE_PARITY_BIT =>
				if clk_counter = baud_period then
					next_state <= STATE_STOP_BITS;
				else
					next_state <= STATE_PARITY_BIT;
				end if;
			when STATE_STOP_BITS_CLEAR_BIT_COUNTER =>
				next_state <= STATE_STOP_BITS;
			when STATE_STOP_BITS =>
				if bit_counter = stop_bits then
					next_state <= STATE_DONE;
				else
					next_state <= STATE_STOP_BITS;
				end if;
				-- pragma translate_off
				assert (bit_counter <= stop_bits)
					report "bit_counter beyond number of stop bits"
						severity failure;
				-- pragma translate_on
			when STATE_DONE =>
				next_state <= STATE_IDLE;
		end case;
	end process;

	state_transition_seq : process (clk, rst)
	begin
		if rst = '1' then
			state <= STATE_IDLE;
		elsif rising_edge(clk) then
			state <= next_state;
		end if;
	end process;

	output_seq : process (clk, rst, next_state)
	begin
		if rst = '1' then
			ready			<= '0';
			busy			<= '0';
			done			<= '0';
			tx				<= '1';
			clk_counter_rst	<= '0';
			clk_counter_en	<= '0';
			bit_counter_rst	<= '0';
			bit_counter_en	<= '0';
		elsif rising_edge(clk) then
			case next_state is
				when STATE_IDLE =>
					ready			<= '1';
					busy			<= '0';
					done			<= '0';
					tx				<= '1';
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_START_BIT =>
					ready			<= '0';
					busy			<= '1';
					done			<= '0';
					tx				<= '0';
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '0';
				when STATE_DATA_BITS =>
					ready			<= '0';
					busy			<= '1';
					done			<= '0';
					tx				<= data(bit_counter);
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '1';
				when STATE_PARITY_BIT =>
					ready			<= '0';
					busy			<= '1';
					done			<= '0';
					tx				<= not (xor data(data_length downto 0)) when parity_odd else xor data(data_length downto 0);
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_STOP_BITS_CLEAR_BIT_COUNTER =>
					ready			<= '0';
					busy			<= '1';
					done			<= '0';
					tx				<= '1';
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
				when STATE_STOP_BITS =>
					ready			<= '0';
					busy			<= '1';
					done			<= '0';
					tx				<= '1';
					clk_counter_rst	<= '0';
					clk_counter_en	<= '1';
					bit_counter_rst	<= '0';
					bit_counter_en	<= '1';
				when STATE_DONE =>
					ready			<= '0';
					busy			<= '0';
					done			<= '1';
					tx				<= '1';
					clk_counter_rst	<= '1';
					clk_counter_en	<= '0';
					bit_counter_rst	<= '1';
					bit_counter_en	<= '0';
			end case;
		end if;
	end process;

	data_register_seq : process (clk, rst)
	begin
		if rst = '1' then
			data	<= (others => '0');
		elsif rising_edge(clk) and (en = '1' and busy = '0') then
			data	<= data_in;
		end if;
	end process;

	clk_counter_seq : process (
		clk,
		rst,
		clk_counter_rst,
		clk_counter_en
		)
	begin
		if rst = '1' then
			clk_counter <= 0;
		elsif rising_edge(clk) then
			if clk_counter_rst = '1' or clk_counter = baud_period then
				clk_counter <= clk_counter'subtype'low;
			elsif clk_counter_en = '1' then
				clk_counter <= clk_counter + 1;
			end if;
		end if;
	end process;
	assert (clk_counter <= baud_period)
		report "clk_counter went out of range baud_period"
			severity failure;

	bit_counter_seq : process (
		clk,
		rst,
		bit_counter_rst,
		bit_counter_en,
		clk_counter
		)
	begin
		if rst = '1' then
			bit_counter <= 0;
		elsif rising_edge(clk) then
			if bit_counter_rst = '1' then
				bit_counter <= bit_counter'subtype'low;
			elsif bit_counter_en = '1' and clk_counter = baud_period then
				if bit_counter = bit_counter'subtype'high then
					bit_counter <= bit_counter'subtype'low;
				else
					bit_counter <= bit_counter + 1;
				end if;
			end if;
		end if;
	end process;

end architecture;
