library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_loopback is
	port (
		rst			: in  std_logic;
		rx			: in  std_logic;
		tx			: out std_logic;
		debug_leds	: out std_logic_vector(1 to 8)
	);
end entity;

architecture structural of uart_loopback is

	component OSCH
		-- synthesis translate_off
		generic (
		NOM_FREQ	: string	:= "3.69"
		);
		-- synthesis translate_on
		port (
		stdby		: in  std_logic;
		osc			: out std_logic;
		sedstdby	: out std_logic
		);
	end component;

	attribute NOM_FREQ	: string;
	attribute NOM_FREQ of OSCH_inst	: label is "3.69";

	signal clk		: std_logic;

	signal data		: std_logic_vector(8 downto 0);

	signal tx_en			: std_logic;
	signal tx_ready			: std_logic;
	signal tx_busy			: std_logic;
	signal tx_done			: std_logic;
	signal rx_clear			: std_logic;
	signal rx_ready			: std_logic;
	signal rx_busy			: std_logic;
	signal rx_done			: std_logic;
	signal rx_err_parity	: std_logic;
	signal rx_err_frame		: std_logic;

begin

	OSCH_inst : OSCH
		-- synthesis translate_off
		generic map (
			NOM_FREQ	=> "3.69"
		)
		-- synthesis translate_on
		port map (
			stdby		=> '0',
			osc			=> clk,
			sedstdby	=> open
		);

	UART_TX_inst : entity work.uart_tx (rtl)
		generic map(
			MAX_BAUD_PERIOD	=> 32
		)
		port map (
			clk			=> clk,
			rst			=> rst,
			data_in		=> data,
			en			=> tx_en,
			ready		=> tx_ready,
			busy		=> tx_busy,
			done		=> tx_done,
			tx			=> tx,
			baud_period	=> 32,
			data_length	=> 8,
			stop_bits	=> 1,
			parity		=> true,
			parity_odd	=> true
		);
		tx_en	<= '1' when rx_ready = '1' and rx_clear = '1' and tx_busy = '0'
						else '0';

	UART_RX_inst : entity work.uart_rx (rtl)
		generic map(
			MAX_BAUD_PERIOD		=> 32,
			SYNCHRONIZER_LENGTH	=> 3
		)
		port map (
			clk			=> clk,
			rst			=> rst,
			data_out	=> data,
			clear		=> rx_clear,
			ready		=> rx_ready,
			err_parity	=> rx_err_parity,
			err_frame	=> rx_err_frame,
			busy		=> rx_busy,
			done		=> rx_done,
			rx			=> rx,
			baud_period	=> 32,
			data_length	=> 8,
			stop_bits	=> 1,
			parity		=> true,
			parity_odd	=> true
		);
		rx_clear	<=	'1' when (rx_ready = '1' or rx_err_parity = '1' or rx_err_frame = '1') and tx_busy = '0'
							else '0';

	debug_leds	<= (1 => tx, 2 => rx, others => '1');


end architecture;
