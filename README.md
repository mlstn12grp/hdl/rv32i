RISC-V SoC Project
==================

[TOC]

## Installation & Setup

### RISC-V Toolchain
This builds and installs the RISC-V RV32IMA toolchain, i.e. 32-bit with ISA extensions I, M, and A.

#### Ubuntu Dependencies
```sh
sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev
```

#### Building the toolchain
```sh
git clone https://github.com/riscv/riscv-tools.git
cd riscv-tools
git submodule update --recursive --init
RISCV='<path/to/riscv/installation>' ./build-rv32ima.sh
```

### GHDL VHDL simulator
```sh
sudo apt-get install gnat-7
git clone https://github.com/ghdl/ghdl.git
cd ghdl
git checkout ghdl-0.35
./configure --prefix=/usr/local
make
sudo make install
```

* GHDL binary will be located at `/usr/local/bin/ghdl`
* VHDL libraries and scripts will be located at `/usr/local/lib/ghdl`

### VUnit VHDL Test Framework
All VHDL test benches are compiled and executed by VUnit.
```sh
sudo apt-get install python-pip
pip install vunit_hdl
```

### SRecord
SRecord will be needed once you generate memory initialization files for the Lattice FPGA.
```sh
sudo apt-get install srecord
```

### Lattice Diamond Software
Lattice Semiconductor's Development Software for FPGA designs.
* Install dependencies
    ```sh
    sudo apt-get install rpm rpm2cpio libusb-0.1-4
    ```
* Download Lattice Diamond Software (RPM)
* Extract the files and the post-install scriptlets
    ```sh
    mkdir diamond
    cd diamond
    rpm2cpio *.rpm | cpio -idmv
    rpm -qp --scripts *.rpm
    ```
* Highlight and copy the postinstall section, then open a text editor and paste the contents into a file named _postin.sh_
* Make the file executable and run it
    ```sh
    chmod +x postin.sh
    RPM_INSTALL_PREFIX=$PWD/usr/local bash postin.sh
    ```
* Copy subdirectory `usr/local/diamond` to its final location
* Set up UDEV rules in order to be able to program the FPGA boards. The purpose of the following new rule in `/etc/udev/rules.d/10-lattice.rules` is to have the _ftdi_sio_ driver unbind from the attached FPGA board. Paste the following in the indicated file.
    ```
    # Lattice FTDI programmer
    SUBSYSTEM == "usb", DRIVER == "usb", \
                        ATTRS{idVendor} == "0403", ATTRS{idProduct} == "6010", \
                        MODE := "0666", RUN += "/bin/sh -c 'basename %p:1.0 > /sys/bus/usb/drivers/ftdi_sio/unbind'"
    ```
* Reload UDEV
    ```sh
    sudo udevadm control --reload
    ```

### `.bashrc` Environment
Append this to your `.bashrc` file
```sh
# RISC-V Development Environment
export RISCV_SOC_PROJECT_BASE="<path/to/project/root>"
export RISCV_BASE="<path/to/riscv/installation>"
export GHDL_LIB_BASE="/usr/local/lib/ghdl"
export LATTICE_DIAMOND_BASE="<path/to/lattice/diamond/root/directory>" # e.g. ... /diamond/3_10_x64
alias lattice_diamond='$LATTICE_DIAMOND_BASE/bin/lin64/diamond'
alias open_riscv_project='cd $RISCV_SOC_PROJECT_BASE'
```

### Build Lattice vendor libraries
This is required to simulate Lattice IP components.
```sh
cd <path/to/project/root>
cd lib
make lattice_vendor_library
```

## Work with the repository

### Compile test software
Only compile and link the ELF `test.elf`
```sh
cd <path/to/project/root>
cd software
make
```

Generate the SREC file `test.srec` that the simulator loads to initialize the program ROM for simulations **not** using EBR-based ROM (see below)
```sh
cd <path/to/project/root>
cd software
make srec
```

Generate a Lattice Memory Initialization File `test.mem` if you want to simulate the design with EBR-based ROM.
```sh
cd <path/to/project/root>
cd software
make lattice_mem_init_file
```
**NOTE**: Use the file `test.mem` in the IPExpress GUI to initialize the EBR-based ROM

### Run VHDL tests
Run all tests in all test benches
```sh
cd <path/to/project/root>
cd test
make
```

Run selected tests
```sh
cd <path/to/project/root>
cd test
make lib.*
make lib.rtl_tb.*
make lib.rtl_tb.test_rtl
```
