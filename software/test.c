#include <csr.h>

#define FAIL_TEST() do {*((unsigned int *)0x00000000) = 0xdeadbeef;} while(0)

int mult (int a, unsigned int b);
int mult (int a, unsigned int b) {
	int sum = 0;
	for (unsigned int i = 0; i < b; i++){
		sum += a;
	}
	return sum;
}

int main(void){
	int a = 1;
	int b = 2;
	int c;

	//--------------------------------------------------------------------------
	// add
	c = a + b;
	if (c != 3) {
		FAIL_TEST();
	}
	// sub
	c = a - b;
	// sll
	c = a << b;
	// srl
	c = ((unsigned int) a) >> b;
	// sra (because a and b are integers)
	c = a >> b;
	// and
	c = a & b;
	// or
	c = a | b;
	// xor
	c = a ^ b;
	// slt
	c = a < b;
	c = a > b;
	// sltu
	c = ((unsigned int) a < (unsigned int) b);
	c = ((unsigned int) a > (unsigned int) b);
	c = 31;
	a = (a << c) + 1;
	c = ((unsigned int) a < (unsigned int) b);
	c = ((unsigned int) a > (unsigned int) b);
	c = 31;
	b = (a << c) + 2;
	c = ((unsigned int) a < (unsigned int) b);
	c = ((unsigned int) a > (unsigned int) b);
	a = 1;
	b = 2;
	//--------------------------------------------------------------------------
	// addi
	c = a + (unsigned int)((1 << 11) - 1);
	// addi negative
	c = a - 2048;
	// slli
	c = a << 5;
	// srli
	a = 1 << 7;
	c = ((unsigned int) a) >> 5;
	// srai
	c = a >> 5;
	// andi
	c = a & 0xFF;
	// ori
	c = a | 0xFF;
	// xori
	c = a ^ 0xFF;
	// slti
	a = 1;
	c = a < 2;
	c = a < -1;
	// sltiu
	c = ((unsigned int) a) < 2;
	c = 31;
	a = (a << c) + 1;
	c = ((unsigned int) a) < 2;
	//--------------------------------------------------------------------------
	// loop and function call
	for (int i = 0; i < 10; i++) {
		c = a + i;
	}

	a = 10;
	b = 10;
	c = mult(a, (unsigned int) b);

	//--------------------------------------------------------------------------
	// Access CSRs
	// unsigned int csr_val;
	// unsigned int val = 0xcafeaffe;
	// csr_swap(csr_val, 0, val);
	// csr_read(csr_val, cycle);
	// csr_write(1, val);
	// csr_read_set(csr_val, 2, val);
	// csr_set(3, val);
	// csr_read_clear(csr_val, 4, val);
	// csr_clear(5, val);

	//--------------------------------------------------------------------------
	// Loads and stores of finer granularity
	char x = 'a';
	short y = (short) x;
	y = y + x;

	// infinite loop triggers end of test
	while(1){}
}
