#ifndef _ASM_RISCV_CSR_H_
#define _ASM_RISCV_CSR_H_

#define csr_swap(out, csr, val)												\
({																			\
	unsigned int __v = (unsigned int)(val);									\
	__asm__ __volatile__ ("csrrw %0, " #csr ", %1"							\
							: "=r" (__v) : "rK" (__v));						\
	out = __v;																\
})																			\

#define csr_read(out, csr)													\
({																			\
	register unsigned int __v;												\
	__asm__ __volatile__ ("csrr %0, " #csr : "=r" (__v));					\
	out = __v;																\
})																			\

#define csr_write(csr, val)													\
({																			\
	unsigned int __v = (unsigned int)(val);									\
	__asm__ __volatile__ ("csrw " #csr ", %0" : : "rK" (__v));				\
})																			\

#define csr_read_set(out, csr, val)											\
({																			\
	register unsigned int __v = (unsigned int)(val);						\
	__asm__ __volatile__ ("csrrs %0, " #csr ", %1"							\
							: "=r" (__v) : "rK" (__v));						\
	out = __v;																\
})																			\

#define csr_set(csr, val)													\
({																			\
	unsigned int __v = (unsigned int)(val);									\
	__asm__ __volatile__ ("csrs " #csr ", %0" : : "rK" (__v));				\
})																			\

#define csr_read_clear(out, csr, val)										\
({																			\
	register unsigned int __v = (unsigned int)(val);						\
	__asm__ __volatile__ ("csrrc %0, " #csr ", %1"							\
							: "=r" (__v) : "rK" (__v));						\
	out = __v;																\
})																			\

#define csr_clear(csr, val)													\
({																			\
	unsigned int __v = (unsigned int)(val);									\
	__asm__ __volatile__ ("csrc " #csr ", %0" : : "rK" (__v));				\
})																			\

#endif /* _ASM_RISCV_CSR_H_ */
