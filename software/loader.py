import sys
import serial

def printProgressBarWithMessage(value, endvalue, message="", bar_length=20):

	percent = float(value) / endvalue
	arrow = '-' * int(round(percent * bar_length)-1) + '>'
	spaces = ' ' * (bar_length - len(arrow))

	sys.stdout.write("\rProgress: [{0}] {1}%".format(arrow + spaces, int(round(percent * 100))) + " | " + message)
	sys.stdout.flush()

ser = serial.Serial(port='/dev/ttyUSB1', baudrate=19200, bytesize=8, parity=serial.PARITY_EVEN, stopbits=2, timeout=1)
print(ser.name)

resp = str()

print("Init loop")
while resp != "ERR\r\n":
	print("written: %d") % (ser.write(b'x\r'))
	resp = ser.readline()
	print(resp)

resp = str()

print("Entering HOLD mode")
while resp != "OK\r\n":
	print("written: %d") % (ser.write(b'hold\r'))
	resp = ser.readline()
	print(resp)

print("Opening firmware Srecord")
firmware_file = open("./test.srec", "r")
all_lines = firmware_file.readlines()
printProgressBarWithMessage(0, len(all_lines), "Starting ...", 50)
for i in range(len(all_lines)):
	# print(line)
	line = all_lines[i].strip()
	if (line[0:2] == "S1"):
		# print("Programming ...")
		resp = str()
		while resp != "OK\r\n":
			# print("written: %d") % (ser.write(b'prog' + line.strip()[4:].decode("hex") + '\r'))
			ser.write(b'prog' + line[4:].decode("hex") + '\r')
			resp = ser.readline()
			printProgressBarWithMessage(i, len(all_lines), line + ": " + resp.strip(), 50)
	else:
		printProgressBarWithMessage(i, len(all_lines), line, 50)
			# print(resp)
print("")
print("Programming complete")
print("----")
firmware_file.close()

resp = str()

print("Exiting HOLD mode")
while resp != "OK\r\n":
	print("written: %d") % (ser.write(b'exit\r'))
	resp = ser.readline()
	print(resp)


print("The END")
exit(0)
