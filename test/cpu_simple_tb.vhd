--------------------------------------------------------------------------------
--! @file cpu_simple_tb.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------
--! @todo Add watchdog timeout provided by VUnit
--------------------------------------------------------------------------------

library vunit_lib;
context vunit_lib.vunit_context;

library ieee;
use ieee.std_logic_1164.all;

use work.riscv_isa_pkg.all;
use work.cpu_register_pkg.all;
use work.cpu_memory_pkg.all;
use work.cpu_test_pkg.all;

entity simple_tb is
  generic (runner_cfg : string);
end entity;

architecture tb of simple_tb is

	component processor
		port (
			clk			: in  std_ulogic;
			rst			: in  std_ulogic;
			test_output	: out test_output_t
		);
	end component;

	----------------------------------------------------------------------------

  constant instruction_memory_init : instruction_memory_record_t := (
	  (op_add, 3, 1, 1, 0),
	  (op_add, 4, 2, 2, 0),
	  (op_sub, 5, 2, 1, 0),
	  (op_sub, 6, 1, 2, 0),
	  (op_and, 12, 30, 31, 0), -- should be 0x00
	  (op_and, 13, 30, 30, 0), -- should be 0x55
	  (op_or, 14, 30, 31, 0), -- should be 0xFF
	  (op_or, 15, 31, 31, 0), -- should be 0xAA
	  (op_xor, 16, 30, 31, 0), -- should be 0xFF
	  (op_xor, 17, 30, 30, 0), -- should be 0x00
	  (op_jal, 9, 0, 0, 0),
	  (op_lw, 7, 0, 0, 0),
	  (op_lw, 8, 0, 0, 1),
	  -- (op_bne, 0, 3, 4, 2),
	  (op_bne, 0, 0, 0, 2),
	  (op_sw, 0, 0, 4, 2),
	  (op_jal, 11, 0, 0, 0),
	  (op_jal, 10, 0, 0, -5),
	  others => (unknown, 0, 0, 0, 0)
  );

  constant register_file_init : register_file_t := (
	  1 => 1,
	  2 => 2,
	  30 => 16#55#,
	  31 => 16#AA#,
	  others => 0
  );

  constant data_memory_init : data_memory_t := (
	  0 => 500,
	  1 => 1024,
	  others => 0
  );

	constant CLK_PERIOD	: time	:= 50 ns;
	constant RST_PERIOD	: time	:= 500 ns;
	signal clk			: std_ulogic;
	signal rst			: std_ulogic;

  signal test_output	: test_output_t;

	----------------------------------------------------------------------------

	for dut : processor
		use entity work.cpu (behavioral)
			generic map (
				register_file_init		=> register_file_init,
				instruction_memory_init	=> instruction_memory_init,
				data_memory_init		=> data_memory_init
			)
			port map (
				clk					=> clk,
				rst.global			=> rst,
				rst.firmware_loader	=> '0',
				hold				=> '0',
				test_output			=> test_output
			);

begin

	dut : processor
	  port map (
		  clk			=> clk,
		  rst			=> rst,
		  test_output	=> test_output
	  );

  main : process
  begin
	test_runner_setup(runner, runner_cfg);
	-- report "Hello world!";

	while test_suite loop

	  if run("simple_test") then
		report "Test started";
		wait until test_output.test_end = true;
		wait for 1 ns;
	  end if;

	end loop;

	test_runner_cleanup(runner); -- Simulation ends here
  end process;

	clk_gen : process
	begin
		clk	<= '0';
		wait for CLK_PERIOD/2;
		clk	<= '1';
		wait for CLK_PERIOD/2;
	end process;

	rst_gen : process
	begin
		rst <= '1';
		wait for RST_PERIOD;
		rst <= '0';
		wait;
	end process;
end architecture;
