--------------------------------------------------------------------------------
--! @file cpu_program_tb.vhd
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------
--! @todo Assert hold signal multiple times with random number of cycles
--! between assertions and random number of cycles in duration.
--
--! @todo Add watchdog timeout provided by VUnit
--
--! @todo The path to compiled program shall be provided to the test bench
--! as a generic.
--------------------------------------------------------------------------------

library vunit_lib;
context vunit_lib.vunit_context;

library ieee;
use ieee.std_logic_1164.all;

use work.cpu_instruction_decode_pkg.all;
use work.cpu_register_pkg.all;
use work.cpu_memory_pkg.all;
use work.cpu_test_pkg.all;

entity model_tb is
	generic (runner_cfg : string);
end entity;

architecture tb of model_tb is

	component processor
		port (
			clk			: in  std_ulogic;
			rst			: in  std_ulogic;
			hold		: in  std_ulogic;
			test_output	: out test_output_t
		);
	end component;

	----------------------------------------------------------------------------

	constant instruction_memory_init	: instruction_memory_record_t	:= import_srec(filepath => "../software/test.srec", debug => true);
	constant register_file_init			: register_file_t				:= (others => 0);
	constant data_memory_init			: data_memory_t					:= (others => 0);

	constant CLK_PERIOD	: time	:= 50 ns;
	constant RST_PERIOD	: time	:= 500 ns;
	signal clk			: std_ulogic;
	signal rst			: std_ulogic;
	signal hold			: std_ulogic;

	signal test_output	: test_output_t;

	----------------------------------------------------------------------------

	for dut : processor
		use entity work.cpu (behavioral)
			generic map (
				register_file_init		=> register_file_init,
				instruction_memory_init	=> instruction_memory_init,
				data_memory_init		=> data_memory_init
			)
			port map (
				clk					=> clk,
				rst.global			=> rst,
				rst.firmware_loader	=> '0',
				hold				=> hold,
				test_output			=> test_output
			);

begin

	dut : processor
		port map (
			clk			=> clk,
			rst			=> rst,
			hold		=> hold,
			test_output	=> test_output
		);

	----------------------------------------------------------------------------

	main : process is
	begin
	test_runner_setup(runner, runner_cfg);

	wait for 1 ns;

	if run("test_model") then
		report "Test: running compiled program";
		wait until test_output.test_end = true;
		wait for 1 ns;
	end if;

	wait for 1 ns;

	test_runner_cleanup(runner); -- Simulation ends here
	end process;

	clk_gen : process
	begin
		clk	<= '0';
		wait for CLK_PERIOD/2;
		clk	<= '1';
		wait for CLK_PERIOD/2;
	end process;

	rst_gen : process
	begin
		rst <= '1';
		wait for RST_PERIOD;
		rst <= '0';
		wait;
	end process;

	hold_period : process
	begin
		hold	<= '0';
		wait until rst = '0';
		for i in 1 to 100 loop
			wait until rising_edge(clk);
		end loop;
		hold	<= '1';
		for i in 1 to 100 loop
			wait until rising_edge(clk);
		end loop;
		hold	<= '0';
		wait;
	end process;

end architecture;
