--------------------------------------------------------------------------------
--! @file cpu_simple_tb.vhdl
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library vunit_lib;
context vunit_lib.vunit_context;

library ieee;
use ieee.std_logic_1164.all;

entity uart_tb is
  generic (runner_cfg : string);
end entity;

architecture tb of uart_tb is

	constant CLK_PERIOD	: time	:= 50 ns;
	constant RST_PERIOD	: time	:= 500 ns;
	signal clk			: std_ulogic;
	signal rst			: std_ulogic;

	signal data		: std_logic_vector(8 downto 0);
	signal en		: std_logic;
	signal ready	: std_logic;
	signal busy		: std_logic;
	signal done		: std_logic;
	signal tx_rx	: std_logic;

begin

  dut_tx : entity work.uart_tx (rtl)
	generic map (
		MAX_BAUD_PERIOD	=> 16
	)
	port map (
		clk			=> clk,
		rst			=> rst,
		data_in		=> data,
		en			=> en,
		ready		=> ready,
		busy		=> busy,
		done		=> done,
		tx			=> tx_rx,
		baud_period	=> 16,
		data_length	=> data'length - 1,
		stop_bits	=> 1,
		parity		=> true,
		parity_odd	=> false
	);

  dut_rx : entity work.uart_rx (rtl)
	generic map (
		MAX_BAUD_PERIOD		=> 16,
		SYNCHRONIZER_LENGTH	=> 3
	)
	port map (
		clk			=> clk,
		rst			=> rst,
		data_out	=> open,
		clear		=> '0',
		ready		=> open,
		busy		=> open,
		done		=> open,
		rx			=> tx_rx,
		baud_period	=> 16,
		data_length	=> data'length - 1,
		stop_bits	=> 1,
		parity		=> true,
		parity_odd	=> false
	);

  main : process
  begin
	test_runner_setup(runner, runner_cfg);
	-- report "Hello world!";

	while test_suite loop

	  if run("simple_test") then
		report "Test started";
		data	<= "0" & "01011100";
		wait until rst = '0';
		wait until rising_edge(clk);
		en		<= '1';
		wait until busy = '1';
		wait until rising_edge(clk);
		en		<= '0';
		wait until done = '1';
		wait until done = '0';
		wait for 1 ns;
	  end if;

	end loop;

	test_runner_cleanup(runner); -- Simulation ends here
  end process;

	clk_gen : process
	begin
		clk	<= '0';
		wait for CLK_PERIOD/2;
		clk	<= '1';
		wait for CLK_PERIOD/2;
	end process;

	rst_gen : process
	begin
		rst <= '1';
		wait for RST_PERIOD;
		rst <= '0';
		wait;
	end process;
end architecture;
