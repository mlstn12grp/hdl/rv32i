from os.path import join, dirname
from os import walk

from vunit import VUnit

# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Set source and testbench path
src_root_path = join(dirname(__file__), "../src")
src_tree = walk(src_root_path)
tb_path = dirname(__file__)

# Create library 'lib'
lib = vu.add_library("lib")

# Add pre-compiled libraries
machxo2_lib = vu.add_external_library("machxo2", join(dirname(__file__), "../lib/vendor_libraries/lattice/machxo2", "v08"))

# Add all files ending in .vhd in current working directory to library
for directory in src_tree:
    directory_path = directory[0]
    lib.add_source_files(join(directory_path, "*.vhd"), allow_empty = True)
lib.add_source_files(join(tb_path, "*.vhd"))
lib.set_compile_option("ghdl.flags", ["-frelaxed-rules", "--ieee=synopsys", "--no-vital-checks"])

vu.set_sim_option("ghdl.elab_flags", ["-frelaxed-rules", "-v", "--ieee=synopsys"])

# Add OSVVM
vu.add_osvvm()

# Run vunit function
vu.main()
