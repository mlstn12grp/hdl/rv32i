--------------------------------------------------------------------------------
--! @file cpu_simple_tb.vhdl
--
--! @author Alexander Preissner <alexander.l.preissner@gmail.com>
--! @date 2018
--! @copyright 2018 Alexander Preissner
--------------------------------------------------------------------------------

library vunit_lib;
context vunit_lib.vunit_context;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity firmware_loader_tb is
  generic (runner_cfg : string);
end entity;

architecture tb of firmware_loader_tb is

	alias ch is character;

	type cmd_array_t is array (positive range <>, positive range <>) of character;
	constant cmd_array	: cmd_array_t	:= (
		('h', 'o', 'l', 'd', cr, lf, nul, nul, nul, nul, nul, nul, nul, ack),
		('p', 'r', 'o', 'g', 'a',            'b',            '0',           '1',             '2',            '3',            ch'val(16#6f#), cr, lf, ack),
		('p', 'r', 'o', 'g', 'e',            'f',            '4',           '5',             '6',            '7',            ch'val(16#57#), cr, lf, ack),
		('p', 'r', 'o', 'g', ch'val(16#ff#), ch'val(16#04#), ch'val(16#13#), ch'val(16#01#), ch'val(16#ff#), ch'val(16#10#), ch'val(16#d2#), cr, lf, ack),
		('p', 'r', 'o', 'g', ch'val(16#ff#), ch'val(16#04#), ch'val(16#13#), ch'val(16#01#), ch'val(16#ff#), ch'val(16#10#), ch'val(16#d1#), cr, lf, nak),
		('e', 'x', 'i', 't', cr, lf, nul, nul, nul, nul, nul, nul, nul, ack),
		('h', 'i', 'l', 'd', cr, lf, nul, nul, nul, nul, nul, nul, nul, nak)
	);

	constant UART_BAUD_PERIOD	: positive	:= 32;

	constant CLK_PERIOD	: time	:= 50 ns;
	constant RST_PERIOD	: time	:= 500 ns;
	signal clk			: std_ulogic;
	signal rst			: std_ulogic;

	signal data		: std_logic_vector(8 downto 0);
	signal en		: std_logic;
	signal ready	: std_logic;
	signal busy		: std_logic;
	signal done		: std_logic;
	signal tx_rx	: std_logic;
	signal clear	: std_logic;
	signal rx_ready	: std_logic;

	signal fl_uart_rx	: std_logic;
	signal fl_uart_tx	: std_logic;

begin

  uart_tx_inst : entity work.uart_tx (rtl)
	generic map (
		MAX_BAUD_PERIOD	=> UART_BAUD_PERIOD
	)
	port map (
		clk			=> clk,
		rst			=> rst,
		data_in		=> data,
		en			=> en,
		ready		=> ready,
		busy		=> busy,
		done		=> done,
		tx			=> fl_uart_rx,
		baud_period	=> UART_BAUD_PERIOD,
		data_length	=> data'length - 1,
		stop_bits	=> 2,
		parity		=> true,
		parity_odd	=> false
	);

	firmware_loader_inst : entity work.firmware_loader (rtl)
		generic map (
			UART_BAUD_PERIOD	=> UART_BAUD_PERIOD
		)
		port map (
			clk		=> clk,
			rst		=> rst,
			uart_rx	=> fl_uart_rx,
			uart_tx	=> fl_uart_tx
		);

  uart_rx_inst : entity work.uart_rx (rtl)
	generic map (
		MAX_BAUD_PERIOD		=> UART_BAUD_PERIOD,
		SYNCHRONIZER_LENGTH	=> 3
	)
	port map (
		clk			=> clk,
		rst			=> rst,
		data_out	=> open,
		clear		=> clear,
		ready		=> rx_ready,
		busy		=> open,
		done		=> open,
		rx			=> fl_uart_tx,
		baud_period	=> UART_BAUD_PERIOD,
		data_length	=> data'length - 1,
		stop_bits	=> 2,
		parity		=> true,
		parity_odd	=> false
	);

  main : process
  begin
	test_runner_setup(runner, runner_cfg);
	-- report "Hello world!";

	while test_suite loop

		if run("firmware_loader_test") then
			report "Test started";
			en		<= '0';
			clear	<= '0';
			wait until rst = '0';
			for i in cmd_array'range(1) loop
				for j in cmd_array'range(2) loop
					if cmd_array(i, j) /= nul and cmd_array(i, j) /= ack and cmd_array(i, j) /= nak then
						data	<= "0" & std_logic_vector(to_unsigned(character'pos(cmd_array(i, j)), 8));
						wait until rising_edge(clk);
						en		<= '1';
						wait until busy = '1';
						wait until rising_edge(clk);
						en		<= '0';
						wait until done = '1';
						wait until done = '0';
						wait for 1 us;
					elsif cmd_array(i, j) = ack then
						for k in 1 to 4 loop
							if not rx_ready = '1' then
								wait until rx_ready = '1';
							end if;
							clear	<= '1';
							wait until rising_edge(clk);
							clear	<= '0';
							wait until rising_edge(clk);
						end loop;
					elsif cmd_array(i, j) = nak then
						for k in 1 to 5 loop
							if not rx_ready = '1' then
								wait until rx_ready = '1';
							end if;
							clear	<= '1';
							wait until rising_edge(clk);
							clear	<= '0';
							wait until rising_edge(clk);
						end loop;
					end if;
				end loop;
			end loop;
			wait for 10 us;
		end if;

	end loop;

	test_runner_cleanup(runner); -- Simulation ends here
  end process;
  test_runner_watchdog(runner, 2 ms);

	clk_gen : process
	begin
		clk	<= '0';
		wait for CLK_PERIOD/2;
		clk	<= '1';
		wait for CLK_PERIOD/2;
	end process;

	rst_gen : process
	begin
		rst <= '1';
		wait for RST_PERIOD;
		rst <= '0';
		wait;
	end process;
end architecture;
